AnimalsSearch.Views.AddAnimalView = Backbone.View.extend({
    el: ".container",
    template: _.template($('#add-animal-template').html()),
    allKinds: new AnimalsSearch.Collections.AllKinds(),
    allBreeds: new AnimalsSearch.Collections.BreedList(),
    allCriteria: new AnimalsSearch.Collections.CriteriaList(),
    allLocations: new AnimalsSearch.Collections.LocationList(),

    render: function () {
        var that = this;
        this.status = 'LOST';
        this.breed = new AnimalsSearch.Models.Breed();
        this.kind = new AnimalsSearch.Models.Kind();
        this.location = new AnimalsSearch.Models.Location();
        this.photoList = [];
        this.age = null;
        this.allKinds.fetch({
            success: function (allKinds) {
                that.allKinds = allKinds;
                that.kind = allKinds.models[0];

                that.allBreeds.fetch({
                    success: function (allBreeds) {
                        that.currentBreeds = new AnimalsSearch.Collections.BreedList();
                        that.allBreeds = allBreeds;

                        var currentBreedsId = that.kind.get('breedIdList');
                        var breed;

                        for (var i = 0; i < that.allBreeds.length; i++) {
                            breed = that.allBreeds.models[i];
                            for (var y = 0; y < currentBreedsId.length; y++) {
                                if (breed.get('id') == currentBreedsId[y]) {
                                    that.currentBreeds.models[that.currentBreeds.length++] = breed;
                                    break;
                                }
                            }

                            if (that.currentBreeds.length == currentBreedsId.length) {
                                break;
                            }
                        }

                        that.breed = null;

                        that.allCriteria.fetch({
                            success: function (allCriteria) {
                                that.chosenCriteria = new AnimalsSearch.Collections.CriteriaList();
                                that.allCriteria = allCriteria;

                                that.allLocations.fetch({
                                    success: function (allLocations) {
                                        that.location = allLocations.models[0];

                                        that.$el.html(that.template({
                                            kinds: allKinds.toJSON(), criteriaList: allCriteria.toJSON(),
                                            breeds: that.currentBreeds.toJSON(), locations: allLocations.toJSON()
                                        }));
                                    }
                                });
                            }

                        });
                    }
                });
            }
        });
    },

    events: {
        'change #selectKind': 'changeKind',
        'click #addButton': 'addAnimalAction',
        'click .checkbox': 'addCriteria',
        'change #animalAge': 'addAge',
        'change #selectBreed': 'selectBreed',
        'change #selectStatus': 'selectStatus',
        'click #addNewCriteria': 'addNewCriteria',
        'click #addNewKind': 'addNewKind',
        'click #addNewBreed': 'addNewBreed',
        'click #addNewLocation': 'addNewLocation',
        'change #selectLocation': 'selectLocation'
    },

    addAnimalAction: function () {
        var that = this;

        if (this.breed != null) {
            this.breed.set('kind', that.kind);
        }

        var animal = new AnimalsSearch.Models.Animal({
            user: user, kind: that.kind, criteriaList: that.chosenCriteria,
            age: that.age, breed: that.breed, status: that.status, location: that.location
        });

        animal.save({},{
            success: function(){
                swal({
                    title: 'Saved',
                    text: 'Animal was added',
                    type: 'success'
                });
                router.navigate('/', {trigger: true});
            }
        });
    },

    changeKind: function (event) {
        var kind = event.target.value;
        var that = this;
        var i;

        for (i = 0; i < this.allKinds.length; i++) {
            if (that.allKinds.models[i].get('name') == kind) {
                that.kind = that.allKinds.models[i];
                break;
            }
        }

        var currentBreedsId = that.kind.get('breedIdList');
        var breed;
        that.currentBreeds = new AnimalsSearch.Collections.BreedList();

        for (i = 0; i < that.allBreeds.length; i++) {
            breed = that.allBreeds.models[i];
            for (var y = 0; y < currentBreedsId.length; y++) {
                if (breed.id == currentBreedsId[y]) {
                    that.currentBreeds.models[that.currentBreeds.length++] = breed;
                }
            }
        }

        $('#selectBreed')
            .find('option')
            .remove()
            .end();

        $('#selectBreed').append("<option></option>");
        for (i = 0; i < that.currentBreeds.length; i++) {
            $('#selectBreed').append("<option>" + that.currentBreeds.models[i].get('name') + "</option>");
        }


        that.breed = null;
    },

    addCriteria: function (event) {
        var criteriaId = event.target.value;
        var $target = $(event.target);
        var criteria;
        var i;

        if ($target.is(':checked')) {
            for (i = 0; i < this.allCriteria.length; i++) {
                criteria = this.allCriteria.models[i];
                if (criteria.get('id') == criteriaId) {
                    this.chosenCriteria.models[this.chosenCriteria.length++] = criteria;
                    break;
                }
            }
        } else {
            for (i = 0; i < this.chosenCriteria.length; i++) {
                criteria = this.chosenCriteria.models[i];
                if (criteria.get('id') == criteriaId) {
                    this.chosenCriteria.models.splice(i, 1);
                    this.chosenCriteria.length--;
                    break;
                }
            }
        }
    },

    addAge: function (event) {
        this.age = event.target.value;
    },

    selectBreed: function (event) {
        var breed = event.target.value;

        if (breed == '') {
            this.breed = null;
        }
        else {
            for (var i = 0; i < this.allBreeds.length; i++) {
                if (this.allBreeds.models[i].get('name') == breed) {
                    this.breed = this.allBreeds.models[i];
                    break;
                }
            }
        }
    },

    selectStatus: function (event) {
        this.status = event.target.value;
    },

    selectLocation: function (event) {
        var location = event.target.value;

        for (var i = 0; i < this.allLocations.length; i++) {
            if (this.allLocations.models[i].get('name') == location) {
                this.location = this.allLocations.models[i];
                break;
            }
        }
    },

    addNewCriteria: function (e) {
        e.preventDefault();
        var that = this;

        swal({
                title: "Create new Criteria",
                text: "Enter name for new Criteria:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                inputPlaceholder: "Criteria name",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Save',
                cancelButtonText: 'Cancel',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                closeOnCancel: false
            },
            function (result) {
                if (result) {
                    var criteria = new AnimalsSearch.Models.Criteria({name: result});

                    criteria.save({}, {
                        success: function () {
                            successAddingSwal(that,'Criteria',result);
                        },
                        error: function (model,response) {
                            validationError('Criteria', response);
                        }
                    });

                } else if (result === '') {
                    emptyNameErrorSwal();
                }
                else if (!result) {
                    swal.close();
                }
            });
    },

    addNewKind: function (e) {
        e.preventDefault();
        var that = this;

        swal({
                title: "Create new Kind",
                text: "Enter name for new Kind:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                inputPlaceholder: "Kind name",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Save',
                cancelButtonText: 'Cancel',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                closeOnCancel: false
            },
            function (result) {
                if (result) {
                    var kind = new AnimalsSearch.Models.Kind({name: result});

                    kind.save({}, {
                        success: function () {
                            successAddingSwal(that,'Kind',result);
                        },
                        error: function (model, response) {
                            validationError('Kind', response);
                        }
                    });

                } else if (result === '') {
                    emptyNameErrorSwal();
                }
                else if (!result) {
                    swal.close();
                }
            });
    },

    addNewBreed: function (e) {
        e.preventDefault();
        var that = this;

        swal({
                title: "Create new Breed",
                text: "Enter name for new Breed:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                inputPlaceholder: "Breed name",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Save',
                cancelButtonText: 'Cancel',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                closeOnCancel: false
            },
            function (result) {
                if (result) {
                    var breed = new AnimalsSearch.Models.Breed({name: result, kind: that.kind});

                    breed.save({}, {
                        success: function () {
                            successAddingSwal(that,'Breed',result);
                        },
                        error: function (model,response) {
                            validationError('Breed', response);
                        }
                    });

                } else if (result === '') {
                    emptyNameErrorSwal();
                }
                else if (!result) {
                    swal.close();
                }
            });
    },

    addNewLocation: function (e) {
        e.preventDefault();
        var that = this;

        swal({
                title: "Create new Location",
                text: "Enter name for new Location:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                inputPlaceholder: "Location name",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Save',
                cancelButtonText: 'Cancel',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                closeOnCancel: false
            },
            function (result) {
                if (result) {
                    var location = new AnimalsSearch.Models.Location({name: result});

                    location.save({}, {
                        success: function () {
                            successAddingSwal(that,'Location',result);
                        },
                        error: function (model,response) {
                            validationError('Location', response);
                        }
                    });

                } else if (result === '') {
                    emptyNameErrorSwal();
                }
                else if (!result) {
                    swal.close();
                }
            });
    }
});

function validationError(name,response){
    var errorMessage = '';

    if(response.responseText != ''){
        var responseObj = $.parseJSON(response.responseText);
        console.log(responseObj);

        for (var i = 0; i < responseObj.errors.length; i++) {
            errorMessage = errorMessage + (i + 1) + ". " + responseObj.errors[i].field + ': ' +
            responseObj.errors[i].defaultMessage + '\n'
        }
    }
    else{
        errorMessage = name + ' already exists';
    }

    swal({
        title: 'Cancelled',
        text: errorMessage,
        type: 'error'
    })
}

function successAddingSwal(that,name,result){
    swal({
        title: 'Saved',
        text: name + ' ' + result + ' was added',
        type: 'success'
    });
    that.render();
}

function emptyNameErrorSwal(){
    swal({
        title: 'Warning',
        text: "You did not enter name",
        type: 'warning'
    });
}