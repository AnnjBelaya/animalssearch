package ua.belaya.animals.kind.domain;

import ua.belaya.animals.breed.domain.Breed;
import ua.belaya.animals.core.domain.Domain;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "kind")
public class Kind implements Domain{
    @Id
    @GeneratedValue
    @Column(name = "kind_id")
    private Long id;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[A-z][a-z]+")
    @Column(name = "kind_name", nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "kind")
    private List<Breed> breedList = new ArrayList<Breed>();

    public Kind(){}

    public Kind(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Kind kind = (Kind) o;

        if (!name.equals(kind.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Breed> getBreedList() {
        return breedList;
    }

    public void setBreedList(List<Breed> breedList) {
        this.breedList = breedList;
    }
}
