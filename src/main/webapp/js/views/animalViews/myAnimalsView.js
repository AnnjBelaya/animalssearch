AnimalsSearch.Views.MyAnimalsView = Backbone.View.extend({
    el: ".container",
    template: _.template($('#animals-my').html()),
    status: 'ALL',

    render: function (min, max, act) {
        var that = this;
        this.minLinkValue = min;
        this.maxLinkValue = max;
        this.activePGNLink = act;
        var animalData;
        var numberOfAnimalsData;
        var numberOfAnimals;

        if (this.status == 'ALL') {
            this.animals = new AnimalsSearch.Collections.AnimalListByUsername();
            animalData = {page: that.activePGNLink, size: 2, username: user.get('username')};
            numberOfAnimals = new AnimalsSearch.Models.NumberOfAnimalsByUsername();
            numberOfAnimalsData = {username: user.get('username')}
        }
        else {
            this.animals = new AnimalsSearch.Collections.AnimalListByUsernameAndStatus();
            animalData = {page: that.activePGNLink, size: 2, status: that.status, username: user.get('username')};
            numberOfAnimals = new AnimalsSearch.Models.NumberOfAnimalsByStatusAndUsername();
            numberOfAnimalsData = {status: that.status, username: user.get('username')};
        }

        this.animals.fetch({
            data: animalData,
            success: function (animals) {
                numberOfAnimals.fetch({
                    data: numberOfAnimalsData,
                    success: function (numberOfAnimals) {
                        var numberOfPGNLinks = Math.ceil(numberOfAnimals.get('numberOfAnimals') / 2);

                        if (that.maxLinkValue > numberOfPGNLinks) {
                            that.maxLinkValue = numberOfPGNLinks;
                        }

                        that.$el.html(that.template({
                            minLinkValue: that.minLinkValue, maxLinkValue: that.maxLinkValue,
                            activePGNLink: that.activePGNLink, numberOfPGNLinks: numberOfPGNLinks,
                            animalList: animals.toJSON(), username: $("#username").text(), status: that.status
                        }));
                    }
                });
            }
        });
    },

    events: {
        'click .myPage': 'changePage',
        'click #myNextPages': 'nextPages',
        'click #myPrevPages': 'prevPages',
        'click #myAll': 'getAllAnimals',
        'click #myLost': 'getLostAnimals',
        'click #myFound': 'getFoundAnimals',
        'click #mySale': 'getForSaleAnimals',
        'click #myLookFor': 'getLookForAnimals',
        'submit #addPhoto': 'addPhoto',
        'change .inputFile': 'inputFile'
    },

    inputFile: function(event){
        var id = "#file" + event.target.id;

        $(id)
            .find('p')
            .remove()
            .end();

        $(id).append("<p><b>Image to add:</b> ..." + event.target.value.substr(-10, 10) + "</p>");
    },

    addPhoto: function(event){
        var photo = new AnimalsSearch.Models.Photo();
        var data = new FormData();

        data.append('id', event.target[0].id);
        data.append('file',event.target[0].files[0]);

        photo.save({},{
            async: false,
            data: data,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false
        });

        this.render(0, 3, 0);
    },

    getAllAnimals: function () {
        this.status = 'ALL';
        this.render(0, 3, 0);
    },

    getLostAnimals: function () {
        this.status = 'LOST';
        this.render(0, 3, 0);
    },

    getFoundAnimals: function () {
        this.status = 'FOUND';
        this.render(0, 3, 0);
    },

    getForSaleAnimals: function () {
        this.status = 'SALE';
        this.render(0, 3, 0);
    },

    getLookForAnimals: function () {
        this.status = 'LOOK_FOR';
        this.render(0, 3, 0);
    },

    changePage: function (event) {
        this.activePGNLink = Number(event.target.text) - 1;
        this.render(this.minLinkValue, this.maxLinkValue, this.activePGNLink);
    },

    nextPages: function () {
        this.minLinkValue = this.minLinkValue + 3;
        this.maxLinkValue = this.minLinkValue + 3;

        this.render(this.minLinkValue, this.maxLinkValue, this.activePGNLink);
    },

    prevPages: function () {
        this.minLinkValue = this.minLinkValue - 3;
        this.maxLinkValue = this.minLinkValue + 3;

        this.render(this.minLinkValue, this.maxLinkValue, this.activePGNLink);
    }
});