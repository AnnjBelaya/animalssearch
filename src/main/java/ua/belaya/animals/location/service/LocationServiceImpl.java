package ua.belaya.animals.location.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import ua.belaya.animals.core.domain.Domain;
import ua.belaya.animals.core.domain.Domains;
import ua.belaya.animals.core.exception.DomainNotFoundException;
import ua.belaya.animals.core.exception.DuplicateDomainException;
import ua.belaya.animals.location.domain.Location;
import ua.belaya.animals.location.repository.LocationJpaRepository;

import java.util.List;

/**
 * Created by ann on 08.11.2016.
 */
@Service
public class LocationServiceImpl implements LocationService{
    @Autowired
    private LocationJpaRepository locationRepository;

    @Override
    public Location add(Location location) {
        try {
            return locationRepository.save(location);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateDomainException(Domains.LOCATION);
        }
    }

    @Override
    public Location get(Long id) {
        Location location = locationRepository.findOne(id);

        if (location == null) {
            throw new DomainNotFoundException(Domains.LOCATION, id);
        }

        return location;
    }

    @Override
    public Location update(Long id, Location location) {
        try {
            location.setId(id);
            return locationRepository.save(location);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateDomainException(Domains.LOCATION);
        }
    }

    @Override
    public void delete(Long id) {
        Location location = locationRepository.findOne(id);

        if (location == null) {
            throw new DomainNotFoundException(Domains.CRITERIA, id);
        }

        locationRepository.delete(location);
    }

    @Override
    public List<Location> list() {
        return locationRepository.findAll();
    }
}
