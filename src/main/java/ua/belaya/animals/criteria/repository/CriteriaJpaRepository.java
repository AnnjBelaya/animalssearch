package ua.belaya.animals.criteria.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ua.belaya.animals.criteria.domain.Criteria;

@SuppressWarnings("all")
public interface CriteriaJpaRepository extends JpaRepository<Criteria,Long>{
    @Query("select c from Criteria c where c.name = ?1")
    Criteria getByName(String name);

    @Query("SELECT c FROM Criteria c")
    Page<Criteria> list(Pageable pageable);
}
