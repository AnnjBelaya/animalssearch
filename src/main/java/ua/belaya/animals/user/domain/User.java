package ua.belaya.animals.user.domain;

import org.hibernate.validator.constraints.Email;
import ua.belaya.animals.core.domain.Domain;
import ua.belaya.animals.photo.domain.Photo;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Anna Belaya
 */
@Entity
@Table(name = "dbUser")
public class User implements Domain {
    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private Long id;

    @Email
    @Column(name = "user_username", nullable = false, unique = true)
    private String username;

    @Column(name = "user_password", nullable = false)
    private String password;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[A-z][a-z]+")
    @Column(name = "user_first_name")
    private String firstName;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[A-z][a-z]+")
    @Column(name = "user_last_name")
    private String lastName;

    @Size(min = 10, max = 10)
    @Pattern(regexp = "[0-9-]+")
    @Column(name = "user_phone_Number")
    private String phoneNumber;

    @Column(name = "user_role")
    @Enumerated(EnumType.STRING)
    private Role role = Role.ROLE_USER;

    public User() {
    }

    public User(String username, String password, Role role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!username.equals(user.username)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public enum Role {
        ROLE_USER_ACTIVE, ROLE_USER, ROLE_ADMIN
    }
}
