AnimalsSearch.Views.ChangeUserProfileView = Backbone.View.extend({
    el: ".container",
    template: _.template($('#change-user-profile').html()),

    render: function () {
        this.user = user;
        this.$el.html(this.template({user: user.toJSON()}));
    },

    events: {
        'click #saveUser': 'saveUser'
    },

    saveUser: function () {
        var user = new AnimalsSearch.Models.User();
        user.set('id', this.user.get('id'));
        user.set('lastName', $('#lastName').val());
        user.set('firstName', $('#firstName').val());
        user.set('phoneNumber', $('#phoneNumber').val());
        user.set('username', this.user.get('username'));
        user.set('password', $('#password').val());

        user.save({async: false});

        router.navigate('user/profile', {trigger: true});
    }
});