package ua.belaya.animals.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 * Created by ann on 16.11.2016.
 */
@Configuration
public class MailConfig {
    @Value("${login}")
    private String username;
    @Value("${password}")
    private String password;
    @Value("${host}")
    private String host;
    @Value("${port}")
    private Integer port;
    @Value("${protocol}")
    private String protocol;
    @Value("${auth}")
    private String auth;
    @Value("${starttls.enable}")
    private String startTls;

    @Bean
    public JavaMailSender javaMailService() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

        javaMailSender.setHost(host);
        javaMailSender.setPort(port);
        javaMailSender.setUsername(username);
        javaMailSender.setPassword(password);
        javaMailSender.setJavaMailProperties(getMailProperties());

        return javaMailSender;
    }

    private Properties getMailProperties() {
        Properties properties = new Properties();

        properties.put("mail.transport.protocol", protocol);
        properties.put("mail.smtp.auth", auth);
        properties.put("mail.smtp.starttls.enable", startTls);

        return properties;
    }
}
