package ua.belaya.animals.criteria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.belaya.animals.criteria.domain.Criteria;
import ua.belaya.animals.criteria.service.CriteriaService;

import java.util.List;

@RestController
@RequestMapping("/rest/criteria")
public class CriteriaController {
    @Autowired
    private CriteriaService criteriaService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Criteria> add(@RequestBody Criteria criteria) {
        return new ResponseEntity<Criteria>(criteriaService.add(criteria),HttpStatus.OK);
    }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    public ResponseEntity<Criteria> getByName(@PathVariable("name") String name) {
        return new ResponseEntity<Criteria>(criteriaService.getByName(name), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Criteria> get(@PathVariable("id") Long id) {
        return new ResponseEntity<Criteria>(criteriaService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@PathVariable("id") Long id,
                                       @RequestBody Criteria criteria) {
        criteriaService.update(id, criteria);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        criteriaService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Criteria>> list() {
        return new ResponseEntity<List<Criteria>>(criteriaService.list(), HttpStatus.OK);
    }
}
