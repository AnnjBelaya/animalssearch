package ua.belaya.animals.core.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.belaya.animals.core.domain.Domain;

import java.util.List;

/**
 * @author Anna Belaya
 */
public interface DomainService<T extends Domain>{
    T add(T domain);
    T update(Long id, T domain);
    void delete(Long id);
}
