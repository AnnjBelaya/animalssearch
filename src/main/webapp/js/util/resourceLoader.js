AnimalsSearch.Utils.loadResource = function(resourcePath) {
    var resource_dir = './js';
    var resource_url = resource_dir + '/' + resourcePath + '.js';
    var resource_string = '';

    $.ajax({
        url: resource_url,
        method: 'GET',
        async: false,
        contentType: 'text',
        success: function (data) {
            resource_string = data;
        }
    });

    $('body').append('<script src="' + resource_url + '" type="text/javascript"><\/script>');
};