package ua.belaya.animals.breed.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ua.belaya.animals.breed.domain.Breed;

@SuppressWarnings("all")
public interface BreedJpaRepository extends JpaRepository<Breed, Long>{
    @Query("select b from Breed b where b.name = ?1")
    Breed getByName(String name);

    @Query("SELECT a FROM Breed a")
    Page<Breed> list(Pageable pageable);
}
