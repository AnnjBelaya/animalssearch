package ua.belaya.animals.location.service;

import ua.belaya.animals.core.service.DomainService;
import ua.belaya.animals.location.domain.Location;

import java.util.List;

/**
 * Created by ann on 08.11.2016.
 */
public interface LocationService extends DomainService<Location>{
    Location get(Long id);

    List<Location> list();
}
