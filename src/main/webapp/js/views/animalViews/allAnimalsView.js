AnimalsSearch.Views.AllAnimalsView = Backbone.View.extend({
    el: ".container",
    template: _.template($('#animals-all').html()),
    status: 'ALL',
    kinds: new AnimalsSearch.Collections.AllKinds(),
    kind: new AnimalsSearch.Models.Kind(),
    locations: new AnimalsSearch.Collections.LocationList(),
    location: new AnimalsSearch.Models.Location(),

    render: function (min, max, act) {
        var that = this;
        this.minLinkValue = min;
        this.maxLinkValue = max;
        this.activePGNLink = act;

        this.kinds.fetch({
            success: function (kinds) {
                if(!that.kind.length){
                    that.kind = kinds.models[0].get('name');
                }

                that.locations.fetch({
                    success: function (locations) {
                        if(!that.location.length){
                            that.location = locations.models[0].get('name');
                        }

                        var animalData;
                        var numberOfAnimalsData;
                        var numberOfAnimals;
                        var animals;

                        if (that.status == 'ALL') {
                            animalData = {page: that.activePGNLink, size: 2, kind: that.kind, location: that.location};
                            numberOfAnimalsData = {kind: that.kind, location: that.location};
                            animals = new AnimalsSearch.Collections.AnimalList();
                            numberOfAnimals = new AnimalsSearch.Models.NumberOfAnimals();
                        }
                        else {
                            animalData = {
                                page: that.activePGNLink,
                                size: 2,
                                status: that.status,
                                kind: that.kind,
                                location: that.location
                            };
                            numberOfAnimalsData = {status: that.status, kind: that.kind, location: that.location};
                            animals = new AnimalsSearch.Collections.AnimalListByStatus();
                            numberOfAnimals = new AnimalsSearch.Models.NumberOfAnimalsByStatus();
                        }

                        animals.fetch({
                            data: animalData,
                            success: function (animals) {
                                numberOfAnimals.fetch({
                                    data: numberOfAnimalsData,
                                    success: function (numberOfAnimals) {
                                        var numberOfPGNLinks = Math.ceil(numberOfAnimals.get('numberOfAnimals') / 2);

                                        if (that.maxLinkValue > numberOfPGNLinks) {
                                            that.maxLinkValue = numberOfPGNLinks;
                                        }

                                        that.$el.html(that.template({
                                            minLinkValue: that.minLinkValue, maxLinkValue: that.maxLinkValue,
                                            activePGNLink: that.activePGNLink, numberOfPGNLinks: numberOfPGNLinks,
                                            animalList: animals.toJSON(), username: user.get('username'), status: that.status,
                                            kinds: that.kinds.toJSON(), actKind: that.kind, locations: that.locations.toJSON(),
                                            actLocation: that.location
                                        }));
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    },

    events: {
        'click .page': 'changePage',
        'click #nextPages': 'nextPages',
        'click #prevPages': 'prevPages',
        'click #all': 'getAllAnimals',
        'click #lost': 'getLostAnimals',
        'click #found': 'getFoundAnimals',
        'click #sale': 'getForSaleAnimals',
        'click #lookFor': 'getLookForAnimals',
        'change #selectKindFilter': 'getKindFilter',
        'change #selectLocationFilter': 'getLocationFilter'
    },

    getKindFilter: function (event) {
        this.kind = event.target.value;
        this.render(0, 3, 0);
    },

    getLocationFilter: function (event) {
        this.location = event.target.value;
        this.render(0, 3, 0);
    },

    getAllAnimals: function () {
        this.status = 'ALL';
        this.render(0, 3, 0);
    },

    getLostAnimals: function () {
        this.status = 'LOST';
        this.render(0, 3, 0);
    },

    getFoundAnimals: function () {
        this.status = 'FOUND';
        this.render(0, 3, 0);
    },

    getForSaleAnimals: function () {
        this.status = 'SALE';
        this.render(0, 3, 0);
    },

    getLookForAnimals: function () {
        this.status = 'LOOK_FOR';
        this.render(0, 3, 0);
    },

    changePage: function (event) {
        this.activePGNLink = Number(event.target.text) - 1;
        this.render(this.minLinkValue, this.maxLinkValue, this.activePGNLink);
    },

    nextPages: function () {
        this.minLinkValue = this.minLinkValue + 3;
        this.maxLinkValue = this.minLinkValue + 3;

        this.render(this.minLinkValue, this.maxLinkValue, this.activePGNLink);
    },

    prevPages: function () {
        this.minLinkValue = this.minLinkValue - 3;
        this.maxLinkValue = this.minLinkValue + 3;

        this.render(this.minLinkValue, this.maxLinkValue, this.activePGNLink);
    }
});