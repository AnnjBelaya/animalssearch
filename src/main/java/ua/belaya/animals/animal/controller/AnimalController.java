package ua.belaya.animals.animal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ua.belaya.animals.animal.domain.Animal;
import ua.belaya.animals.animal.domain.AnimalDTO;
import ua.belaya.animals.animal.service.AnimalService;
import ua.belaya.animals.criteria.domain.Criteria;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/rest/animal")
public class AnimalController {
    @Autowired
    private AnimalService animalService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Animal> add(@Valid @RequestBody Animal animal) {
        return new ResponseEntity<Animal>(animalService.add(animal), HttpStatus.OK);
    }

    @RequestMapping(value = "/photo", headers = "content-type=multipart/form-data", method = RequestMethod.POST)
    public ResponseEntity<Void> addPhoto(@PathParam("id") Long id,
                                         @RequestParam("file") MultipartFile multipartFile) {
        animalService.addPhoto(id, multipartFile);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/breedName/{breedName}", method = RequestMethod.GET)
    public ResponseEntity<List<AnimalDTO>> getByBreedName(@PathVariable("breedName") String kindName) {
        return new ResponseEntity<List<AnimalDTO>>(animalService.getByBreedName(kindName), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<AnimalDTO> get(@PathVariable("id") Long id) {
        return new ResponseEntity<AnimalDTO>(animalService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@PathVariable("id") Long id,
                                       @Valid @RequestBody Animal animal) {
        animalService.update(id, animal);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        animalService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/criteria", method = RequestMethod.POST)
    public ResponseEntity<Void> addCriteriaList(@PathVariable("id") Long id,
                                                @PathParam("criteriaIdList") Set<Long> criteriaIdList) {
        animalService.addCriteriaList(id, criteriaIdList);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/criteria")
    public ResponseEntity<List<Criteria>> criteriaList(@PathVariable("id") Long id) {
        return new ResponseEntity<List<Criteria>>(animalService.criteriaList(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/numberOfAnimals")
    public ResponseEntity<Map<String, Long>> numberOfAnimalsByKindAndLocation(@PathParam("kind") String kind,
                                                                              @PathParam("location") String location) {
        Map<String, Long> map = new HashMap<String, Long>();
        map.put("numberOfAnimals", animalService.numberOfAnimalsByKindAndLocation(kind, location));
        return new ResponseEntity<Map<String, Long>>(map, HttpStatus.OK);
    }

    @RequestMapping(value = "/numberOfAnimals/status")
    public ResponseEntity<Map<String, Long>> numberOfAnimalsByKindAndLocation(@PathParam("status") Animal.Status status,
                                                                              @PathParam("kind") String kind,
                                                                              @PathParam("location") String location) {
        Map<String, Long> map = new HashMap<String, Long>();
        map.put("numberOfAnimals", animalService.numberOfAnimalsByKindAndLocation(status, kind, location));
        return new ResponseEntity<Map<String, Long>>(map, HttpStatus.OK);
    }

    @RequestMapping(value = "/filtered/status/all")
    public ResponseEntity<List<AnimalDTO>> filteredList(@PathParam("status") Animal.Status status,
                                                        @PathParam("animalId") Long animalId,
                                                        @PathParam("username") String username) {
        return new ResponseEntity<List<AnimalDTO>>(animalService.filteredList(status, username, animalId), HttpStatus.OK);
    }

    @RequestMapping(value = "/filtered/status/two/all")
    public ResponseEntity<List<AnimalDTO>> filteredlist(@PathParam("firstStatus") Animal.Status firstStatus,
                                                        @PathParam("secondStatus") Animal.Status secondStatus,
                                                        @PathParam("username") String username,
                                                        @PathParam("animalId") Long animalId) {
        return new ResponseEntity<List<AnimalDTO>>(
                animalService.filteredList(firstStatus, secondStatus, username, animalId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<AnimalDTO>> listByKindAndLocation(@PathParam("page") int page,
                                                                 @PathParam("size") int size,
                                                                 @PathParam("kind") String kind,
                                                                 @PathParam("location") String location) {
        return new ResponseEntity<List<AnimalDTO>>(
                animalService.listByKindAndLocation(new PageRequest(page, size), kind, location), HttpStatus.OK);
    }

    @RequestMapping(value = "/status")
    public ResponseEntity<List<AnimalDTO>> listByKindAndLocation(@PathParam("page") int page,
                                                                 @PathParam("size") int size,
                                                                 @PathParam("status") Animal.Status status,
                                                                 @PathParam("kind") String kind,
                                                                 @PathParam("location") String location) {
        return new ResponseEntity<List<AnimalDTO>>(
                animalService.listByKindAndLocation(new PageRequest(page, size), status, kind, location), HttpStatus.OK);
    }

    @RequestMapping(value = "/username")
    public ResponseEntity<List<AnimalDTO>> listByUsername(@PathParam("page") int page,
                                                          @PathParam("size") int size,
                                                          @PathParam("username") String username) {
        return new ResponseEntity<List<AnimalDTO>>(
                animalService.listByUsername(new PageRequest(page, size), username), HttpStatus.OK);
    }

    @RequestMapping(value = "/username/status")
    public ResponseEntity<List<AnimalDTO>> listByUsername(@PathParam("page") int page,
                                                          @PathParam("size") int size,
                                                          @PathParam("status") Animal.Status status,
                                                          @PathParam("username") String username) {
        return new ResponseEntity<List<AnimalDTO>>(
                animalService.listByUsername(new PageRequest(page, size), status, username), HttpStatus.OK);
    }

    @RequestMapping(value = "/numberOfAnimals/username")
    public ResponseEntity<Map<String, Long>> numberOfAnimals(@PathParam("username") String username) {
        Map<String, Long> map = new HashMap<String, Long>();
        map.put("numberOfAnimals", animalService.numberOfAnimals(username));
        return new ResponseEntity<Map<String, Long>>(map, HttpStatus.OK);
    }

    @RequestMapping(value = "/numberOfAnimals/username/status")
    public ResponseEntity<Map<String, Long>> numberOfAnimals(@PathParam("status") Animal.Status status,
                                                             @PathParam("username") String username) {
        Map<String, Long> map = new HashMap<String, Long>();
        map.put("numberOfAnimals", animalService.numberOfAnimals(status, username));
        return new ResponseEntity<Map<String, Long>>(map, HttpStatus.OK);
    }
}
