package ua.belaya.animals.core.domain;

/**
 * @author Anna Belaya
 */
public enum Domains {
    USER("User"), KIND("Kind"), ANIMAL("Animal"), CRITERIA("Criteria"), BREED("Breed"),
    LOCATION("Location"), PHOTO("Photo");

    private String name;

    Domains(String domainName) {
        name = domainName;
    }

    public String getName() {
        return name;
    }
}
