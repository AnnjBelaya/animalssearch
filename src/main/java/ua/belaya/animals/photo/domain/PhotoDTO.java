package ua.belaya.animals.photo.domain;

import java.io.File;

/**
 * Created by ann on 21.11.2016.
 */
public class PhotoDTO {
    private Long id;

    private String photo;

    private Long animalId;

    public PhotoDTO(Photo photo){
        this.id = photo.getId();
        this.photo = photo.getPhoto();
        this.animalId = photo.getAnimal().getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Long getAnimalId() {
        return animalId;
    }

    public void setAnimalId(Long animalId) {
        this.animalId = animalId;
    }
}
