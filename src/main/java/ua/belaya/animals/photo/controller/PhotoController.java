package ua.belaya.animals.photo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.belaya.animals.photo.domain.Photo;
import ua.belaya.animals.photo.domain.PhotoDTO;
import ua.belaya.animals.photo.service.PhotoService;

/**
 * Created by ann on 21.11.2016.
 */
@RestController
@RequestMapping("/rest/photo")
public class PhotoController {
    @Autowired
    private PhotoService photoService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Photo> add(@RequestBody Photo photo) {
        return new ResponseEntity<Photo>(photoService.add(photo), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<PhotoDTO> get(@PathVariable("id") Long id) {
        return new ResponseEntity<PhotoDTO>(photoService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@PathVariable("id") Long id,
                                       @RequestBody Photo photo) {
        photoService.update(id, photo);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        photoService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
