package ua.belaya.animals.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ua.belaya.animals.email.MessageSender;
import ua.belaya.animals.core.domain.Domains;
import ua.belaya.animals.core.exception.DomainNotFoundException;
import ua.belaya.animals.core.exception.DuplicateDomainException;
import ua.belaya.animals.user.domain.User;
import ua.belaya.animals.user.repository.UserJpaRepository;

/**
 * @author Anna Belaya
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserJpaRepository userRepository;
    @Autowired
    private MessageSender messageSender;

    @Override
    public User getAuthorized() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        if(username.equals("anonymousUser")){
            throw new DomainNotFoundException();
        }

        return userRepository.getByUsername(username);
    }

    @Override
    public User add(User user) {
        try {
            userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateDomainException(Domains.USER);
        }

        return user;
    }

    @Override
    public User getByUsername(String username) {
        User user = userRepository.getByUsername(username);

        if (user == null) {
            throw new DomainNotFoundException(Domains.USER, username);
        }

        return user;
    }

    @Override
    public User get(Long id) {
        User user = userRepository.findOne(id);

        if (user == null) {
            throw new DomainNotFoundException(Domains.USER, id);
        }

        return user;
    }

    @Override
    public User update(Long id, User user) {
        try {
            user.setId(id);
            User authorizedUser = getAuthorized();

            if(user.equals(authorizedUser)){
                user.setRole(authorizedUser.getRole());
            }

            userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateDomainException(Domains.USER);
        }

        return user;
    }

    @Override
    public void delete(Long id) {
        if (!userRepository.exists(id)) {
            throw new DomainNotFoundException(Domains.USER, id);
        }

        userRepository.delete(id);
    }

    @Override
    public Page<User> list(Pageable pageable) {
        return userRepository.list(pageable);
    }

    @Override
    public User sendMessage(Long id, String subject, String text) {
        User user = userRepository.findOne(id);

        if (user == null){
            throw new DomainNotFoundException(Domains.USER, id);
        }

        messageSender.sendMessage(subject, text, user.getUsername());

        return user;
    }

    @Override
    public void activate(Long id) {
        User user = userRepository.findOne(id);

        if (user == null){
            throw new DomainNotFoundException(Domains.USER, id);
        }

        user.setRole(User.Role.ROLE_USER_ACTIVE);

        userRepository.save(user);
    }

    public void setMessageSender(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    public void setUserRepository(UserJpaRepository userRepository) {
        this.userRepository = userRepository;
    }
}
