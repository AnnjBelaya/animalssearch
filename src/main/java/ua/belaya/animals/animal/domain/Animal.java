package ua.belaya.animals.animal.domain;

import ua.belaya.animals.breed.domain.Breed;
import ua.belaya.animals.core.domain.Domain;
import ua.belaya.animals.criteria.domain.Criteria;
import ua.belaya.animals.kind.domain.Kind;
import ua.belaya.animals.location.domain.Location;
import ua.belaya.animals.photo.domain.Photo;
import ua.belaya.animals.user.domain.User;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "animal")
public class Animal implements Domain {
    @Id
    @GeneratedValue
    @Column(name = "animal_id")
    private Long id;

    @ManyToMany()
    @JoinTable(name = "animal_criteria",
            joinColumns = {@JoinColumn(name = "animal_id")},
            inverseJoinColumns = {@JoinColumn(name = "criteria_id")})
    private List<Criteria> criteriaList = new ArrayList<Criteria>();

    @Min(1)
    @Column(name = "animal_age")
    private Integer age;

    @ManyToOne
    @JoinColumn(name = "animal_dbUser", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "animal_kind", nullable = false)
    private Kind kind;

    @ManyToOne
    @JoinColumn(name = "animal_breed")
    private Breed breed;

    @Column(name = "user_role")
    @Enumerated(EnumType.STRING)
    private Status status = Status.FOUND;

    @ManyToOne
    @JoinColumn(name = "animal_location", nullable = false)
    private Location location;

    @OneToMany(mappedBy = "animal")
    private List<Photo> photoList = new ArrayList<Photo>();

    public Animal() {
    }

    public Animal(User user, Integer age) {
        this.user = user;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Criteria> getCriteriaList() {
        return criteriaList;
    }

    public void setCriteriaList(List<Criteria> criteriaList) {
        this.criteriaList = criteriaList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }

    public Breed getBreed() {
        return breed;
    }

    public void setBreed(Breed breed) {
        this.breed = breed;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Photo> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<Photo> photoList) {
        this.photoList = photoList;
    }

    public enum Status {
        LOST, FOUND, SALE, LOOK_FOR
    }
}
