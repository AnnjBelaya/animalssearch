package ua.belaya.animals.animal.domain;

/**
 * Created by ann on 26.10.2016.
 */
public class AnimalCount implements Comparable<AnimalCount> {
    private Animal animal;
    private int count;

    public AnimalCount(Animal animal, int count) {
        this.animal = animal;
        this.count = count;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public int compareTo(AnimalCount animalCount) {
        if (count > animalCount.getCount()) {
            return -1;
        }

        if (count < animalCount.getCount()){
            return 1;
        }

        return 0;
    }
}
