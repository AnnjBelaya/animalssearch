AnimalsSearch.Views.AnotherUserProfileView = Backbone.View.extend({
    el: ".container",
    template: _.template($('#another-user-profile').html()),

    render: function (id) {
        var user = new AnimalsSearch.Models.User({id: id});
        var that = this;

        user.fetch({
            success: function (user) {
                if ($("#username").text() == user.get('username')) {
                    router.navigate('user/profile', {trigger: true});
                }

                that.$el.html(that.template({user: user.toJSON()}));
            }
        });
    }
});