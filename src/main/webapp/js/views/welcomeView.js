AnimalsSearch.Views.Welcome = Backbone.View.extend({
    el: ".container",
    template: _.template($('#welcome').html()),

    render: function(){
        if(user.get('firstName')){
            this.greeting = "Welcome, Dear " + user.get('firstName') + "!"
            this.role = user.get('role');

            this.email = user.get('username');
        }
        else{
            this.greeting = "Welcome!";
            this.role = 'none';
        }

        this.$el.html(this.template({greeting : this.greeting, role: this.role, email: this.email}));
    },

    events:{
        'click #getActMessage' : 'getActMessage'
    },

    getActMessage: function(){
        var message = new AnimalsSearch.Models.Message;
        message.set({id: user.get("id")});

        message.fetch({
            data: {subject: 'authentication', text: authMessage(user)},
            success: function () {
                swal({
                    title: "Authentication message was sent on your Email " +
                    user.get("username"),
                    text: "Please follow the link in the message to " +
                    "authorise your account.",
                    imageUrl: '../resources/img/ok.jpg'
                });
            }
        });
    }
});

function authMessage(user) {
    var url = "\"http://localhost:8080/#user/activate/" + user.get('id') + "\"";
    return '<div><a href=' + url + '>Get authorised with AnimalsSearch!</a></div>'
}