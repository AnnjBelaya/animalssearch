package ua.belaya.animals.kind.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ua.belaya.animals.kind.domain.Kind;


@SuppressWarnings("all")
public interface KindJpaRepository extends JpaRepository<Kind, Long>{
    @Query("select k from Kind k where k.name = ?1")
    Kind getByName(String name);

    @Query("SELECT k FROM Kind k")
    Page<Kind> list(Pageable pageable);
}
