package ua.belaya.animals.animal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ua.belaya.animals.animal.domain.Animal;
import ua.belaya.animals.animal.domain.AnimalCount;
import ua.belaya.animals.animal.domain.AnimalDTO;
import ua.belaya.animals.animal.repository.AnimalJpaRepository;
import ua.belaya.animals.breed.domain.Breed;
import ua.belaya.animals.core.domain.Domains;
import ua.belaya.animals.core.exception.ContradictoryDataException;
import ua.belaya.animals.core.exception.DomainNotFoundException;
import ua.belaya.animals.core.exception.DuplicateDomainException;
import ua.belaya.animals.criteria.domain.Criteria;
import ua.belaya.animals.criteria.repository.CriteriaJpaRepository;
import ua.belaya.animals.kind.domain.Kind;
import ua.belaya.animals.photo.domain.Photo;
import ua.belaya.animals.photo.repository.PhotoJpaRepository;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Service
public class AnimalServiceImpl implements AnimalService {
    @Autowired
    private AnimalJpaRepository animalRepository;
    @Autowired
    private CriteriaJpaRepository criteriaRepository;
    @Autowired
    private PhotoJpaRepository photoRepository;

    @Override
    public Animal add(Animal animal) {
        Breed breed = animal.getBreed();
        Kind kind = animal.getKind();

        if (breed != null && !kind.equals(breed.getKind())) {
            throw new ContradictoryDataException(breed, kind);
        }

        try {
            return animalRepository.save(animal);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateDomainException(Domains.ANIMAL);
        }
    }

    @Override
    public void addPhoto(Long id, MultipartFile multipartFile) {
        Animal animal = animalRepository.findOne(id);

        if (animal == null) {
            throw new DomainNotFoundException(Domains.ANIMAL, id);
        }

        try {
            File file = new File(System.getProperty("user.dir") +
                    "\\src\\main\\webapp\\resources\\photo\\" + Math.random() * 5 +
                    multipartFile.getOriginalFilename());

            multipartFile.transferTo(file);

            photoRepository.save(new Photo(file.getName(), animal));
        }catch (IOException e){}
    }

    @Override
    public List<AnimalDTO> getByBreedName(String breedName) {
        List<AnimalDTO> animalDTOList = new ArrayList<AnimalDTO>();

        for (Animal animal : animalRepository.getByBreedName(breedName)) {
            animalDTOList.add(new AnimalDTO(animal));
        }

        return animalDTOList;
    }

    @Override
    public AnimalDTO get(Long id) {
        Animal animal = animalRepository.findOne(id);

        if (animal == null) {
            throw new DomainNotFoundException(Domains.ANIMAL, id);
        }

        return new AnimalDTO(animal);
    }

    @Override
    public Animal update(Long id, Animal animal) {
        try {
            animal.setId(id);
            return animalRepository.save(animal);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateDomainException(Domains.ANIMAL);
        }
    }

    @Override
    public void delete(Long id) {
        Animal animal = animalRepository.findOne(id);

        if (animal == null) {
            throw new DomainNotFoundException(Domains.ANIMAL, id);
        }

        animalRepository.delete(animal);
    }

    @Override
    public List<AnimalDTO> listByKindAndLocation(Pageable pageable, String kind, String location) {
        List<AnimalDTO> animalDTOList = new ArrayList<AnimalDTO>();

        for (Animal animal : animalRepository.listByKindAndLocation(pageable, kind, location)) {
            animalDTOList.add(new AnimalDTO(animal));
        }

        return animalDTOList;
    }

    @Override
    public void addCriteriaList(Long id, Set<Long> criteria) {
        if (!criteria.isEmpty()) {
            Animal animal = animalRepository.findOne(id);

            if (animal == null) {
                throw new DomainNotFoundException(Domains.ANIMAL, id);
            }

            for (Long criteriaId : criteria) {
                animal.getCriteriaList().add(criteriaRepository.findOne(criteriaId));
            }

            animalRepository.save(animal);
        }
    }

    @Override
    public List<Criteria> criteriaList(Long id) {
        Animal animal = animalRepository.findOne(id);

        if (animal == null) {
            throw new DomainNotFoundException(Domains.ANIMAL, id);
        }

        return animal.getCriteriaList();
    }

    @Override
    public long numberOfAnimalsByKindAndLocation(String kind, String location) {
        return animalRepository.numberOfAnimalsByKindAndLocation(kind, location);
    }

    @Override
    public long numberOfAnimalsByKindAndLocation(Animal.Status status, String kind, String location) {
        return animalRepository.numberOfAnimalsByKindAndLocation(status, kind, location);
    }

    @Override
    public List<AnimalDTO> listByKindAndLocation(Pageable pageable, Animal.Status status, String kind, String location) {
        List<AnimalDTO> animalDTOList = new ArrayList<AnimalDTO>();

        for (Animal animal : animalRepository.listByKindAndLocation(pageable, status, kind, location)) {
            animalDTOList.add(new AnimalDTO(animal));
        }

        return animalDTOList;
    }

    @Override
    public List<AnimalDTO> listByUsername(Pageable pageable, String username) {
        List<AnimalDTO> animalDTOList = new ArrayList<AnimalDTO>();

        for (Animal animal : animalRepository.listByUsername(pageable, username)) {
            animalDTOList.add(new AnimalDTO(animal));
        }

        return animalDTOList;
    }

    @Override
    public List<AnimalDTO> listByUsername(Pageable pageable, Animal.Status status, String username) {
        List<AnimalDTO> animalDTOList = new ArrayList<AnimalDTO>();

        for (Animal animal : animalRepository.listByUsername(pageable, status, username)) {
            animalDTOList.add(new AnimalDTO(animal));
        }

        return animalDTOList;
    }

    @Override
    public List<AnimalDTO> filteredList(Animal.Status status, String username, Long animalId) {
        return animalDTOFilteredList(animalRepository.listByUsername(status, username), animalId);
    }

    @Override
    public List<AnimalDTO> filteredList(Animal.Status firstStatus, Animal.Status secondStatus,
                                        String username, Long animalId) {
        return animalDTOFilteredList(animalRepository.list(firstStatus, secondStatus, username), animalId);
    }

    @Override
    public long numberOfAnimals(String username) {
        return animalRepository.numberOfAnimals(username);
    }

    @Override
    public long numberOfAnimals(Animal.Status status, String username) {
        return animalRepository.numberOfAnimals(status, username);
    }

    private List<AnimalDTO> animalDTOFilteredList(List<Animal> animalList, Long animalId) {
        List<AnimalDTO> animalDTOList = new ArrayList<AnimalDTO>();
        List<AnimalCount> animalCountList = formAnimalCountList(animalList, animalRepository.findOne(animalId));

        Collections.sort(animalCountList);

        for (AnimalCount animalCount : animalCountList) {
            animalDTOList.add(new AnimalDTO(animalCount.getAnimal()));
        }

        return animalDTOList;
    }

    private List<AnimalCount> formAnimalCountList(List<Animal> animalList, Animal animal) {
        List<AnimalCount> animalCountList = new ArrayList<AnimalCount>();
        int point;

        for (Animal dbAnimal : animalList) {
            point = 0;
            if (animal.getKind().equals(dbAnimal.getKind()) &&
                    animal.getLocation().equals(dbAnimal.getLocation())) {

                if (animal.getBreed() != null && animal.getBreed().equals(dbAnimal.getBreed())) {
                    point += 100;
                }

                point += countPointForAge(animal.getAge(),dbAnimal.getAge());

                point += countPointForCriteria(animal, dbAnimal);

                animalCountList.add(new AnimalCount(dbAnimal, point));
            }
        }

        return animalCountList;
    }

    private int countPointForCriteria(Animal animal, Animal dbAnimal){
        int point = 0;

        for (Criteria dbAnimalCriteria : dbAnimal.getCriteriaList()) {
            for (Criteria animalCriteria : animal.getCriteriaList()) {
                if (dbAnimalCriteria.equals(animalCriteria)) {
                    point++;
                }
            }
        }

        return point;
    }

    private int countPointForAge(Integer animalAge, Integer dbAnimalAge){
        int point = 0;

        if (animalAge != null && dbAnimalAge != null) {
            if(animalAge >= dbAnimalAge - 1 && animalAge <= dbAnimalAge + 1){
                point += 3;
            }
            else if(animalAge >= dbAnimalAge - 2 && animalAge <= dbAnimalAge + 2){
                point += 2;
            }
            else if(animalAge >= dbAnimalAge - 3 && animalAge <= dbAnimalAge + 3){
                point += 1;
            }
        }

        return point;
    }
}
