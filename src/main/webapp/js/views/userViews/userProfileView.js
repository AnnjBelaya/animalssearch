AnimalsSearch.Views.UserProfileView = Backbone.View.extend({
    el: ".container",
    template: _.template($('#user-profile').html()),

    render: function () {
        this.$el.html(this.template({user: user.toJSON()}));
    },

    events: {
        'click #changeUser': 'changeUser',
        'click #getActivationMessage' : 'getActMessage'
    },

    changeUser: function () {
        router.navigate('user/profile/change', {trigger: true});
    },

    getActMessage: function(){
        var message = new AnimalsSearch.Models.Message;
        message.set({id: user.get("id")});

        message.fetch({
            data: {subject: 'authentication', text: authMessage(user)},
            success: function () {
                swal({
                    title: "Authentication message was sent on your Email " +
                    user.get("username"),
                    text: "Please follow the link in the message to " +
                    "authorise your account.",
                    imageUrl: '../resources/img/ok.jpg'
                });
            }
        });
    }
});