package ua.belaya.animals.core.exception;

import ua.belaya.animals.breed.domain.Breed;
import ua.belaya.animals.kind.domain.Kind;

/**
 * Created by ann on 25.10.2016.
 */
public class ContradictoryDataException extends RuntimeException{
    public ContradictoryDataException(Breed breed, Kind kind){
        super(breed.getName() + " can`t be " + kind);
    }
}
