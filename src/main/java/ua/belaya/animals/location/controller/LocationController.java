package ua.belaya.animals.location.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.belaya.animals.location.domain.Location;
import ua.belaya.animals.location.service.LocationService;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by ann on 08.11.2016.
 */
@RestController
@RequestMapping("/rest/location")
public class LocationController {
    @Autowired
    private LocationService locationService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Location> add(@Valid @RequestBody Location location) {
        return new ResponseEntity<Location>(locationService.add(location), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Location> get(@PathVariable("id") Long id) {
        return new ResponseEntity<Location>(locationService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@PathVariable("id") Long id,
                                       @Valid @RequestBody Location criteria) {
        locationService.update(id, criteria);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        locationService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Location>> list() {
        return new ResponseEntity<List<Location>>(locationService.list(), HttpStatus.OK);
    }
}
