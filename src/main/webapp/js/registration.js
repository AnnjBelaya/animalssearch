$(document).ready(function () {
    AnimalsSearch.Views.RegistrationView = Backbone.View.extend({
        el: '#registrationForm',

        events: {
            'click #registration': 'registration'
        },

        registration: function () {
            var user = new AnimalsSearch.Models.User();

            user.set('username', $("#inputUsername").val());
            user.set('password', $("#inputPassword").val());
            user.set('lastName', $("#inputLastName").val());
            user.set('firstName', $("#inputFirstName").val());
            user.set('phoneNumber', $("#inputPhoneNumber").val());

            user.save({}, {
                async: false,
                success: function (user) {
                    var message = new AnimalsSearch.Models.Message;
                    message.set({id: user.get("id")});

                    message.fetch({
                        data: {subject: 'authentication', text: authMessage(user)},
                        success: function () {
                            swal({
                                title: "Authentication message was sent on your Email " +
                                user.get("username"),
                                text: "Please follow the link in the message to " +
                                "authorise your account.",
                                imageUrl: '../resources/img/ok.jpg'
                            });
                        }
                    });
                },
                error: function (model, responce) {
                    console.log(responce);
                    var errorMessage = '';

                    if(responce.responseText != ''){
                        var responseObj = $.parseJSON(responce.responseText);
                        console.log(responseObj);

                        for (var i = 0; i < responseObj.errors.length; i++) {
                            errorMessage = errorMessage + (i + 1) + ". " + responseObj.errors[i].field + ': ' +
                            responseObj.errors[i].defaultMessage + '\n'
                        }
                    }
                    else{
                        errorMessage = 'Email doesn`t exist or already in use';
                    }

                    swal("Validation error!", errorMessage, "error");
                }
            });
        }
    });

    return new AnimalsSearch.Views.RegistrationView();

    function authMessage(user) {
        var url = "\"http://localhost:8080/#user/activate/" + user.get('id') + "\"";
        return '<div><a href=' + url + '>Get authorised with AnimalsSearch!</a></div>'
    }
});



