package ua.belaya.animals.breed.domain;

import ua.belaya.animals.core.domain.Domain;
import ua.belaya.animals.criteria.domain.Criteria;
import ua.belaya.animals.kind.domain.Kind;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "breed")
public class Breed implements Domain{
    @Id
    @GeneratedValue
    @Column(name = "breed_id")
    private Long id;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[a-zA-Z-\\s]+")
    @Column(name = "breed_name", unique = true)
    private String name;

    @ManyToOne()
    @JoinColumn(name = "kind_id", nullable = false)
    private Kind kind;

    public Breed(){}

    public Breed(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }
}
