AnimalsSearch.Views.FindAnimalView = Backbone.View.extend({
    el: ".container",
    template: _.template($('#find-animal').html()),

    render: function (id, min, max, act) {
        if ((this.animal != null && this.animal.get('id') == id) || id == null) {
            this.$el.html(this.template({
                minLinkValue: this.minLinkValue, maxLinkValue: this.maxLinkValue,
                activePGNLink: this.activePGNLink, numberOfPGNLinks: this.numberOfPGNLinks,
                animalList: this.pageAnimals.toJSON(), username: user.get('username')
            }));
        }
        else {
            var data;
            this.animal = new AnimalsSearch.Models.Animal({id: id});
            var that = this;
            this.pageAnimals = new AnimalsSearch.Collections.AnimalList();
            this.firstAnimalIndex = 0;
            this.lastAnimalIndex = 1;
            this.activePGNLink = act;
            this.maxLinkValue = max;
            this.minLinkValue = min;
            this.id = id;

            this.animal.fetch({
                success: function (animal) {
                    this.status = animal.get('status');

                    if (status == 'LOST') {
                        data = {status: 'FOUND', animalId: id, username: user.get('username')};
                        this.allAnimals = new AnimalsSearch.Collections.AnimalFilteredListByStatusAll();
                    }
                    else if (status == 'FOUND') {
                        data = {
                            firstStatus: 'LOOK_FOR',
                            secondStatus: 'LOST',
                            animalId: id,
                            username: user.get('username')
                        };
                        this.allAnimals = new AnimalsSearch.Collections.AnimalFilteredListByTwoStatusesAll();
                    }
                    else if (status == 'SALE') {
                        data = {status: 'LOOK_FOR', animalId: id};
                        this.allAnimals = new AnimalsSearch.Collections.AnimalFilteredListByStatusAll();
                    }
                    else {
                        data = {
                            firstStatus: 'FOUND',
                            secondStatus: 'SALE',
                            animalId: id,
                            username: user.get('username')
                        };
                        this.allAnimals = new AnimalsSearch.Collections.AnimalFilteredListByTwoStatusesAll();
                    }

                    this.allAnimals.fetch({
                        data: data,
                        success: function (animals) {
                            console.log(animals);

                            that.allAnimals = animals;

                            for (var i = that.firstAnimalIndex; i <= that.lastAnimalIndex; i++) {
                                if (animals.models[i]) {
                                    that.pageAnimals.models[that.pageAnimals.length++] = animals.models[i];
                                }
                                else {
                                    break;
                                }
                            }

                            that.numberOfPGNLinks = Math.ceil(animals.length / 2);

                            if (max > that.numberOfPGNLinks) {
                                that.maxLinkValue = that.numberOfPGNLinks;
                            }

                            that.$el.html(that.template({
                                minLinkValue: that.minLinkValue, maxLinkValue: that.maxLinkValue,
                                activePGNLink: that.activePGNLink, numberOfPGNLinks: that.numberOfPGNLinks,
                                animalList: that.pageAnimals.toJSON(), username: user.get('username')
                            }));
                        }
                    });
                }
            });
        }
    },

    events: {
        'click .foundPage': 'changePage',
        'click #foundNextPages': 'nextPages',
        'click #foundPrevPages': 'prevPages'
    },

    changePage: function (event) {
        var newPGNLink = Number(event.target.text) - 1;
        this.pageAnimals = new AnimalsSearch.Collections.AnimalList();
        var i;

        if (this.activePGNLink < newPGNLink) {
            for (i = this.activePGNLink; i < newPGNLink; i++) {
                this.firstAnimalIndex += 2;
                this.lastAnimalIndex += 2;
            }
        }
        else {
            for (i = this.activePGNLink; i > newPGNLink; i--) {
                this.firstAnimalIndex -= 2;
                this.lastAnimalIndex -= 2;
            }
        }

        this.activePGNLink = newPGNLink;

        for (i = this.firstAnimalIndex; i <= this.lastAnimalIndex; i++) {
            if (this.allAnimals.models[i]) {
                this.pageAnimals.models[this.pageAnimals.length++] = this.allAnimals.models[i];
            }
            else {
                break;
            }
        }

        this.render(this.id, this.minLinkValue, this.maxLinkValue, this.activePGNLink);
    },

    nextPages: function () {
        this.minLinkValue = this.minLinkValue + 3;
        this.maxLinkValue = this.minLinkValue + 3;

        if (this.maxLinkValue > this.numberOfPGNLinks) {
            this.maxLinkValue = this.numberOfPGNLinks;
        }

        this.render(this.id, this.minLinkValue, this.maxLinkValue, this.activePGNLink);
    },

    prevPages: function () {
        this.minLinkValue = this.minLinkValue - 3;
        this.maxLinkValue = this.minLinkValue + 3;

        if (this.maxLinkValue > this.numberOfPGNLinks) {
            this.maxLinkValue = this.numberOfPGNLinks;
        }

        this.render(this.id, this.minLinkValue, this.maxLinkValue, this.activePGNLink);
    }
});