package ua.belaya.animals.kind.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ua.belaya.animals.core.domain.Domains;
import ua.belaya.animals.core.exception.DomainNotFoundException;
import ua.belaya.animals.core.exception.DuplicateDomainException;
import ua.belaya.animals.kind.domain.Kind;
import ua.belaya.animals.kind.domain.KindDTO;
import ua.belaya.animals.kind.repository.KindJpaRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class KindServiceImpl implements KindService{
    @Autowired
    private KindJpaRepository kindRepository;

    @Override
    public Kind add(Kind kind) {
        try{
            return kindRepository.save(kind);
        }
        catch (DataIntegrityViolationException e){
            throw new DuplicateDomainException(Domains.KIND);
        }
    }

    @Override
    public KindDTO getByName(String name){
        Kind kind = kindRepository.getByName(name);

        if (kind == null){
            throw new DomainNotFoundException(Domains.KIND, name);
        }

        return new KindDTO(kind);
    }

    @Override
    public KindDTO get(Long id) {
        Kind kind = kindRepository.findOne(id);

        if(kind == null){
            throw new DomainNotFoundException(Domains.KIND, id);
        }

        return new KindDTO(kind);
    }

    @Override
    public Kind update(Long id, Kind kind) {
        try{
            kind.setId(id);
            return kindRepository.save(kind);
        }
        catch (DataIntegrityViolationException e){
            throw new DuplicateDomainException(Domains.KIND);
        }
    }

    @Override
    public void delete(Long id) {
        Kind kind = kindRepository.findOne(id);

        if (!kindRepository.exists(id)) {
            throw new DomainNotFoundException(Domains.KIND, id);
        }

        kindRepository.delete(kind);
    }

    @Override
    public List<KindDTO> list() {
        List<KindDTO> kindDTOList = new ArrayList<KindDTO>();

        for (Kind kind : kindRepository.findAll()){
            kindDTOList.add(new KindDTO(kind));
        }

        return kindDTOList;
    }

    @Override
    public List<KindDTO> list(Pageable pageable) {
        List<KindDTO> kindDTOList = new ArrayList<KindDTO>();

        for(Kind kind : kindRepository.list(pageable)){
            kindDTOList.add(new KindDTO(kind));
        }

        return kindDTOList;
    }
}
