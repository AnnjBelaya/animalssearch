package ua.belaya.animals.breed.service;

import org.springframework.data.domain.Pageable;
import ua.belaya.animals.breed.domain.Breed;
import ua.belaya.animals.breed.domain.BreedDTO;
import ua.belaya.animals.core.service.DomainService;

import java.util.List;

public interface BreedService extends DomainService<Breed>{
    BreedDTO getByName(String name);

    BreedDTO get(Long id);

    List<BreedDTO> list();

    List<BreedDTO> list(Pageable pageable);
}
