package ua.belaya.animals.animal.service;

import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import ua.belaya.animals.animal.domain.Animal;
import ua.belaya.animals.animal.domain.AnimalDTO;
import ua.belaya.animals.core.service.DomainService;
import ua.belaya.animals.criteria.domain.Criteria;

import java.io.File;
import java.util.List;
import java.util.Set;

public interface AnimalService extends DomainService<Animal>{
    void addPhoto(Long id, MultipartFile multipartFile);

    AnimalDTO get(Long id);

    List<AnimalDTO> getByBreedName(String breedName);

    void addCriteriaList(Long id, Set<Long> criteria);

    List<Criteria> criteriaList(Long id);

    List<AnimalDTO> filteredList(Animal.Status status, String username, Long animalId);

    List<AnimalDTO> filteredList(Animal.Status firstStatus, Animal.Status secondStatus, String username, Long animalId);

    List<AnimalDTO> listByKindAndLocation(Pageable pageable, String kind, String location);

    List<AnimalDTO> listByKindAndLocation(Pageable pageable, Animal.Status status, String kind, String location);

    List<AnimalDTO> listByUsername(Pageable pageable, String username);

    List<AnimalDTO> listByUsername(Pageable pageable, Animal.Status status, String username);

    long numberOfAnimalsByKindAndLocation(String kind, String location);

    long numberOfAnimalsByKindAndLocation(Animal.Status status, String kind, String location);

    long numberOfAnimals(String username);

    long numberOfAnimals(Animal.Status status, String username);
}
