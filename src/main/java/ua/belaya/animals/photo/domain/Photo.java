package ua.belaya.animals.photo.domain;

import ua.belaya.animals.animal.domain.Animal;
import ua.belaya.animals.core.domain.Domain;

import javax.persistence.*;
import java.io.File;

/**
 * Created by ann on 21.11.2016.
 */
@Entity
@Table(name = "photo")
public class Photo implements Domain{
    @Id
    @GeneratedValue
    @Column(name = "photo_id")
    private Long id;

    @Column(name = "photo_photo", nullable = false)
    private String photo;

    @ManyToOne()
    @JoinColumn(name = "animal_id", nullable = false)
    private Animal animal;

    public Photo(){}

    public Photo(String photo, Animal animal) {
        this.photo = photo;
        this.animal = animal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }
}
