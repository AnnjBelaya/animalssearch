package ua.belaya.animals.breed.domain;


public class BreedDTO {
    private Long id;

    private String name;

    private Long kindId;

    public BreedDTO(Breed breed){
        this.id = breed.getId();
        this.name = breed.getName();
        this.kindId = breed.getKind().getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getKindId() {
        return kindId;
    }

    public void setKindId(Long kindId) {
        this.kindId = kindId;
    }
}
