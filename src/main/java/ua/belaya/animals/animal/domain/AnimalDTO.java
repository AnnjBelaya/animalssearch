package ua.belaya.animals.animal.domain;

import ua.belaya.animals.criteria.domain.Criteria;
import ua.belaya.animals.kind.domain.Kind;
import ua.belaya.animals.location.domain.Location;
import ua.belaya.animals.photo.domain.Photo;
import ua.belaya.animals.user.domain.User;

import java.util.ArrayList;
import java.util.List;

public class AnimalDTO {
    private  Long id;

    private List<Criteria> criteriaList = new ArrayList<Criteria>();

    private Integer age;

    private User user;

    private String breedName;

    private String kindName;

    private Animal.Status status;

    private Location location;

    private List<String> photoList = new ArrayList<String>();

    public AnimalDTO(Animal animal){
        this.id = animal.getId();
        this.criteriaList = animal.getCriteriaList();
        this.age = animal.getAge();
        this.user = animal.getUser();

        if(animal.getBreed() != null){
            this.breedName = animal.getBreed().getName();
        }
        else {
            this.breedName = "none";
        }

        this.kindName = animal.getKind().getName();
        this.status = animal.getStatus();
        this.location = animal.getLocation();

        for(Photo photo : animal.getPhotoList()){
            photoList.add(photo.getPhoto());
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBreedName() {
        return breedName;
    }

    public void setBreedName(String breedName) {
        this.breedName = breedName;
    }

    public String getKindName() {
        return kindName;
    }

    public void setKindName(String kindName) {
        this.kindName = kindName;
    }

    public List<Criteria> getCriteriaList() {
        return criteriaList;
    }

    public void setCriteriaList(List<Criteria> criteriaList) {
        this.criteriaList = criteriaList;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Animal.Status getStatus() {
        return status;
    }

    public void setStatus(Animal.Status status) {
        this.status = status;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<String> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<String> photoList) {
        this.photoList = photoList;
    }
}
