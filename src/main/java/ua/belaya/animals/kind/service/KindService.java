package ua.belaya.animals.kind.service;

import org.springframework.data.domain.Pageable;
import ua.belaya.animals.core.service.DomainService;
import ua.belaya.animals.kind.domain.Kind;
import ua.belaya.animals.kind.domain.KindDTO;

import java.util.List;

public interface KindService extends DomainService<Kind> {
    KindDTO get(Long id);

    KindDTO getByName(String name);

    List<KindDTO> list();

    List<KindDTO> list(Pageable pageable);
}
