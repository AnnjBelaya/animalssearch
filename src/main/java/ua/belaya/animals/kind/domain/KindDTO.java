package ua.belaya.animals.kind.domain;


import ua.belaya.animals.breed.domain.Breed;

import java.util.ArrayList;
import java.util.List;

public class KindDTO {
    private Long id;

    private String name;

    private List<Long> breedIdList = new ArrayList<Long>();

    public KindDTO(Kind kind){
        this.id = kind.getId();
        this.name = kind.getName();

        for (Breed breed : kind.getBreedList()){
            breedIdList.add(breed.getId());
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getBreedIdList() {
        return breedIdList;
    }

    public void setBreedIdList(List<Long> breedIdList) {
        this.breedIdList = breedIdList;
    }
}
