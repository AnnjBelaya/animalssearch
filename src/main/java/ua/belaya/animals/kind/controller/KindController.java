package ua.belaya.animals.kind.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.belaya.animals.kind.domain.Kind;
import ua.belaya.animals.kind.domain.KindDTO;
import ua.belaya.animals.kind.service.KindService;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/rest/kind")
public class KindController {
    @Autowired
    private KindService kindService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Kind> add(@Valid @RequestBody Kind kind) {
        return new ResponseEntity<Kind>(kindService.add(kind), HttpStatus.OK);
    }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    public ResponseEntity<KindDTO> getByName(@PathVariable("name") String name) {
        return new ResponseEntity<KindDTO>(kindService.getByName(name), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<KindDTO> get(@PathVariable("id") Long id) {
        return new ResponseEntity<KindDTO>(kindService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@PathVariable("id") Long id,
                                       @Valid @RequestBody Kind kind) {
        kindService.update(id, kind);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        kindService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<KindDTO> list() {
        return kindService.list();
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<KindDTO> list(@PathParam("page") int page, @PathParam("size") int size) {
        return kindService.list(new PageRequest(page, size));
    }
}
