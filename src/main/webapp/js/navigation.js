AnimalsSearch.Utils.loadResource('models/models');
AnimalsSearch.Utils.loadResource('collections/collections');
AnimalsSearch.Utils.loadResource('routers/router');
AnimalsSearch.Utils.loadResource('views/animalViews/addAnimalView');
AnimalsSearch.Utils.loadResource('views/animalViews/allAnimalsView');
AnimalsSearch.Utils.loadResource('views/animalViews/findAnimalView');
AnimalsSearch.Utils.loadResource('views/animalViews/myAnimalsView');
AnimalsSearch.Utils.loadResource('views/userViews/anotherUserProfileView');
AnimalsSearch.Utils.loadResource('views/userViews/userProfileView');
AnimalsSearch.Utils.loadResource('views/userViews/changeUserProfileView');
AnimalsSearch.Utils.loadResource('views/userViews/userListView');
AnimalsSearch.Utils.loadResource('views/welcomeView');

var router = new AnimalsSearch.Routes.Router();
var user;
var animalView = new AnimalsSearch.Views.AddAnimalView();
var userProfileView = new AnimalsSearch.Views.UserProfileView();
var changeUserProfileView = new AnimalsSearch.Views.ChangeUserProfileView();
var userList = new AnimalsSearch.Views.UserListView();
var allAnimalsView = new AnimalsSearch.Views.AllAnimalsView();
var anotherUserProfileView = new AnimalsSearch.Views.AnotherUserProfileView();
var myAnimalsView = new AnimalsSearch.Views.MyAnimalsView();
var findAnimalView = new AnimalsSearch.Views.FindAnimalView();
var welcome = new AnimalsSearch.Views.Welcome();

router.on("route:welcome", function () {
    user = new AnimalsSearch.Models.AuthorizedUser();
    user.fetch({async: false});
    welcome.render();
});

router.on("route:authoriseUser", function (id) {
    user = new AnimalsSearch.Models.AuthoriseUser();
    user.set({id: id});
    user.save({async: false});

    document.location.href = '/logout';
});

router.on("route:anotherUserProfile", function (id) {
    anotherUserProfileView.render(id);
});

router.on("route:allAnimals", function () {
    user = new AnimalsSearch.Models.AuthorizedUser();
    user.fetch({async: false});
    allAnimalsView.render(0, 3, 0);
});

router.on("route:users", function () {
    userList.render();
});

router.on("route:addAnimalPage", function () {
    getAuthorizedUser();
    animalView.render();
});

router.on("route:userProfile", function () {
    getAuthorizedUser();
    userProfileView.render();
});

router.on("route:changeUserProfile", function () {
    getAuthorizedUser();
    changeUserProfileView.render();
});

router.on("route:myAnimals", function () {
    getAuthorizedUser();
    myAnimalsView.render(0, 3, 0);
});

router.on("route:findAnimal", function (id) {
    getAuthorizedUser();
    findAnimalView.render(id, 0, 3, 0);
});

router.on("route:refreshView", function () {
    getAuthorizedUser();
    myAnimalsView.render(0, 3, 0);
});

function getAuthorizedUser() {
    user = new AnimalsSearch.Models.AuthorizedUser();

    user.fetch({
        async: false,
        error: function () {
            document.location.href = '/'
        }
    });
}

Backbone.history.start({
    root: "/"
});