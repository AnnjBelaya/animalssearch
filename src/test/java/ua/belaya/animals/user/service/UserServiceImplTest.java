package ua.belaya.animals.user.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import ua.belaya.animals.core.exception.DomainNotFoundException;
import ua.belaya.animals.core.exception.DuplicateDomainException;
import ua.belaya.animals.email.MessageSender;
import ua.belaya.animals.user.domain.User;
import ua.belaya.animals.user.repository.UserJpaRepository;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Anna Belaya
 */
public class UserServiceImplTest {

    @Mock
    private UserJpaRepository userRepositoryMock;

    @Mock
    private MessageSender messageSenderMock;

    private UserServiceImpl userService;

    private User user;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        userService = new UserServiceImpl();
        userService.setUserRepository(userRepositoryMock);
        userService.setMessageSender(messageSenderMock);

        user = new User("anna", "0000", User.Role.ROLE_ADMIN);
    }

    @Test
    public void given_user_then_when_calling_add_user_added_expected() {
        when(userRepositoryMock.save(user)).thenReturn(user);
        User currentUser = userService.add(user);

        verify(userRepositoryMock, times(1)).save(user);

        assertThat(currentUser, is(user));
    }

    @Test
    public void given_user_to_update_then_when_calling_update_user_updated_expected() {
        User passedUser = new User("anna", "0000", User.Role.ROLE_USER_ACTIVE);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                user.getUsername(), user.getPassword()));

        when(userRepositoryMock.getByUsername(user.getUsername())).thenReturn(user);

        userService.update(100L, passedUser);

        assertThat(passedUser.getRole(), is(User.Role.ROLE_ADMIN));

        verify(userRepositoryMock, times(1)).getByUsername(user.getUsername());
        verify(userRepositoryMock, times(1)).save(user);

        assertThat(user, is(userService.getAuthorized()));
        assertThat(passedUser.getId(), is(100L));
        assertThat(user.getRole(), is(User.Role.ROLE_ADMIN));
        assertThat(passedUser.getRole(), is(User.Role.ROLE_ADMIN));
    }

    @Test
    public void given_id_of_user_then_when_calling_get_user_with_right_index_returned_expected() {
        Long id = 1L;

        when(userRepositoryMock.findOne(id)).thenReturn(user);

        assertThat(userService.get(id), equalTo(user));

        verify(userRepositoryMock, times(1)).findOne(id);
    }

    @Test
    public void given_user_then_when_calling_get_user_with_right_username_returned_expected() {
        when(userRepositoryMock.getByUsername(user.getUsername())).thenReturn(user);

        assertThat(userService.getByUsername(user.getUsername()), equalTo(user));

        verify(userRepositoryMock, times(1)).getByUsername(user.getUsername());
    }

    @Test
    public void given_id_of_user_then_when_calling_delete_user_deleted_expected() {
        Long id = 1L;

        when(userRepositoryMock.exists(id)).thenReturn(true);

        userService.delete(id);

        verify(userRepositoryMock, times(1)).delete(id);
    }

    @SuppressWarnings("unchecked")
    @Test(expected = DuplicateDomainException.class)
    public void given_user_with_duplicated_username_then_when_calling_add_DuplicateDomainException_expected() {
        when(userRepositoryMock.save(user)).thenThrow(DataIntegrityViolationException.class);

        userService.add(user);

        verify(userRepositoryMock, times(1)).save(user);
    }

    @SuppressWarnings("unchecked")
    @Test(expected = DuplicateDomainException.class)
    public void given_user_with_duplicated_username_then_when_calling_update_DuplicateDomainException_expected() {
        when(userRepositoryMock.save(user)).thenThrow(DataIntegrityViolationException.class);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                user.getUsername(), user.getPassword()));

        userService.update(1L, user);

        verify(userRepositoryMock, times(1)).save(user);
    }

    @SuppressWarnings("unchecked")
    @Test(expected = DomainNotFoundException.class)
    public void given_user_with_index_which_can_not_be_in_database_then_when_calling_get_DomainNotFoundException_expected() {
        Long id = 1L;

        when(userRepositoryMock.findOne(id)).thenThrow(DomainNotFoundException.class);

        userService.get(id);

        verify(userRepositoryMock, times(1)).findOne(id);
    }

    @SuppressWarnings("unchecked")
    @Test(expected = DomainNotFoundException.class)
    public void given_user_with_username_which_can_not_be_in_database_then_when_calling_getByNickName_DomainNotFoundException_expected() {
        String username = "anya";

        when(userRepositoryMock.getByUsername(username)).thenThrow(DomainNotFoundException.class);

        userService.getByUsername(username);

        verify(userRepositoryMock, times(1)).getByUsername(username);
    }

    @SuppressWarnings("unchecked")
    @Test(expected = DomainNotFoundException.class)
    public void given_user_id_to_delete_then_when_calling_delete_DomainNotFoundException_expected() {
        Long id = 1L;

        when(userRepositoryMock.findOne(id)).thenThrow(DomainNotFoundException.class);

        userService.delete(id);

        verify(userRepositoryMock, times(1)).findOne(id);
    }
}
