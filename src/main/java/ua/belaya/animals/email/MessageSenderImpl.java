package ua.belaya.animals.email;

import groovy.util.logging.Commons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.Properties;

/**
 * Created by ann on 16.11.2016.
 */
@Component
public class MessageSenderImpl implements MessageSender{
    @Autowired
    private JavaMailSender javaMailSender;
    @Value("${login}")
    private String fromEmail;

    public MessageSenderImpl() {}

    public void sendMessage(String subject, String text, String toEmail) {
        try {
            MimeMessage message = javaMailSender.createMimeMessage();

            message.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
            message.setFrom(new InternetAddress(fromEmail));
            message.setSubject(subject);
            message.setContent(text, "text/html; charset=utf-8");

            javaMailSender.send(message);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
