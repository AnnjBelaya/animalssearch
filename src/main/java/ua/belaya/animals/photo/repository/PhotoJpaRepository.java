package ua.belaya.animals.photo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.belaya.animals.photo.domain.Photo;

/**
 * Created by ann on 21.11.2016.
 */
public interface PhotoJpaRepository extends JpaRepository<Photo, Long>{
}
