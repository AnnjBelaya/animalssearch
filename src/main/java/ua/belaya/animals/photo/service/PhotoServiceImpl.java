package ua.belaya.animals.photo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import ua.belaya.animals.core.domain.Domains;
import ua.belaya.animals.core.exception.DomainNotFoundException;
import ua.belaya.animals.core.exception.DuplicateDomainException;
import ua.belaya.animals.photo.domain.Photo;
import ua.belaya.animals.photo.domain.PhotoDTO;
import ua.belaya.animals.photo.repository.PhotoJpaRepository;

import java.io.File;

/**
 * Created by ann on 21.11.2016.
 */
@Service
public class PhotoServiceImpl implements PhotoService{
    @Autowired
    private PhotoJpaRepository photoRepository;

    @Override
    public Photo add(Photo photo) {
        try{
            return photoRepository.save(photo);
        }
        catch (DataIntegrityViolationException e){
            throw new DuplicateDomainException(Domains.PHOTO);
        }
    }

    @Override
    public PhotoDTO get(Long id) {
        Photo photo = photoRepository.findOne(id);

        if (photo == null){
            throw new DomainNotFoundException(Domains.PHOTO, id);
        }

        return new PhotoDTO(photo);
    }

    @Override
    public Photo update(Long id, Photo photo) {
        try{
            photo.setId(id);
            return photoRepository.save(photo);
        }
        catch (DataIntegrityViolationException e){
            throw new DuplicateDomainException(Domains.PHOTO);
        }
    }

    @Override
    public void delete(Long id) {
        Photo photo = photoRepository.findOne(id);

        if (!photoRepository.exists(id)) {
            throw new DomainNotFoundException(Domains.PHOTO, id);
        }

        photoRepository.delete(photo);
    }
}
