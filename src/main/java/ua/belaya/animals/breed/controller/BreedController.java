package ua.belaya.animals.breed.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.belaya.animals.breed.domain.Breed;
import ua.belaya.animals.breed.domain.BreedDTO;
import ua.belaya.animals.breed.service.BreedService;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/rest/breed")
public class BreedController {
    @Autowired
    private BreedService breedService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Breed> add(@Valid @RequestBody Breed breed) {
        return new ResponseEntity<Breed>(breedService.add(breed), HttpStatus.OK);
    }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    public ResponseEntity<BreedDTO> getByName(@PathVariable("name") String name) {
        return new ResponseEntity<BreedDTO>(breedService.getByName(name), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<BreedDTO> get(@PathVariable("id") Long id) {
        return new ResponseEntity<BreedDTO>(breedService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@PathVariable("id") Long id,
                                       @Valid @RequestBody Breed kind) {
        breedService.update(id, kind);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        breedService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<BreedDTO> list() {
        return breedService.list();
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<BreedDTO> list(@PathParam("page") int page, @PathParam("size") int size) {
        return breedService.list(new PageRequest(page, size));
    }
}
