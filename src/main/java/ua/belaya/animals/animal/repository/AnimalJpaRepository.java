package ua.belaya.animals.animal.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ua.belaya.animals.animal.domain.Animal;

import java.util.List;

@SuppressWarnings("all")
public interface AnimalJpaRepository extends JpaRepository<Animal, Long> {
    @Query("select a from Animal a where a.breed.name = :breed")
    List<Animal> getByBreedName(@Param("breed") String breedName);

    @Query("SELECT a FROM Animal a where a.kind.name = :kind and a.location.name = :location")
    Page<Animal> listByKindAndLocation(Pageable pageable, @Param("kind") String kind, @Param("location") String location);

    @Query("select a from Animal a where a.status = :status and a.kind.name = :kind and a.location.name = :location")
    Page<Animal> listByKindAndLocation(Pageable pageable, @Param("status") Animal.Status status, @Param("kind") String kind,
                                       @Param("location") String location);

    @Query("select a from Animal a where a.status = :status and not a.user.username = :username")
    List<Animal> listByUsername(@Param("status") Animal.Status status, @Param("username") String username);

    @Query("select a from Animal a where a.status = :firstStatus or a.status = :secondStatus and not a.user.username = :username")
    List<Animal> list(@Param("firstStatus") Animal.Status firstStatus, @Param("secondStatus") Animal.Status secondStatus,
                      @Param("username") String username);

    @Query("select a from Animal a where a.user.username = :username")
    Page<Animal> listByUsername(Pageable pageable, @Param("username") String username);

    @Query("select a from Animal a where a.status = :status and a.user.username = :username")
    Page<Animal> listByUsername(Pageable pageable, @Param("status") Animal.Status status, @Param("username") String username);

    @Query("select count(a) from Animal a where a.kind.name = :kind and a.location.name = :location")
    long numberOfAnimalsByKindAndLocation(@Param("kind") String Kind, @Param("location") String location);

    @Query("select count(a) from Animal a where a.status = :status and a.kind.name = :kind and a.location.name = :location")
    long numberOfAnimalsByKindAndLocation(@Param("status") Animal.Status status, @Param("kind") String Kind,
                                          @Param("location") String location);

    @Query("select count(a) from Animal a where a.user.username = :username")
    long numberOfAnimals(@Param("username") String username);

    @Query("select count(a) from Animal a where a.status = :status and a.user.username = :username")
    long numberOfAnimals(@Param("status") Animal.Status status, @Param("username") String username);
}
