package ua.belaya.animals.user.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ua.belaya.animals.user.domain.User;

import java.util.List;

/**
 * @author Anna Belaya
 */
@SuppressWarnings("all")
public interface UserJpaRepository extends JpaRepository<User, Long> {
    @Query("select u from User u where u.username = ?1")
    User getByUsername(String username);

    @Query("SELECT u FROM User u")
    Page<User> list(Pageable pageable);
}
