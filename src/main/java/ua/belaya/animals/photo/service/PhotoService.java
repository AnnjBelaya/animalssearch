package ua.belaya.animals.photo.service;

import ua.belaya.animals.core.service.DomainService;
import ua.belaya.animals.photo.domain.Photo;
import ua.belaya.animals.photo.domain.PhotoDTO;

/**
 * Created by ann on 21.11.2016.
 */
public interface PhotoService extends DomainService<Photo>{
    PhotoDTO get(Long id);
}
