package ua.belaya.animals.email;

/**
 * Created by ann on 16.11.2016.
 */
public interface MessageSender {
    void sendMessage(String subject, String text, String toEmail);
}
