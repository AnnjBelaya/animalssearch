package ua.belaya.animals.user.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.belaya.animals.core.service.DomainService;
import ua.belaya.animals.user.domain.User;

/**
 * @author Anna Belaya
 */
public interface UserService extends DomainService<User>{
    User get(Long id);

    User getByUsername(String username);

    User getAuthorized();

    Page<User> list(Pageable pageable);

    User sendMessage(Long id, String subject, String text);

    void activate(Long id);
}
