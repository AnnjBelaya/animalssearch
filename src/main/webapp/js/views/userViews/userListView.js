AnimalsSearch.Views.UserListView = Backbone.View.extend({
    el: ".container",
    template: _.template($('#users-template').html()),

    render: function () {
        var that = this;
        var users = new AnimalsSearch.Collections.Users();
        users.fetch({
            data: {page: 0, size: 10},
            success: function (users) {
                that.$el.html(that.template({users: users.toJSON()}));
            },
            error: function () {
                console.log("Error!");
            }
        });
    }
});
