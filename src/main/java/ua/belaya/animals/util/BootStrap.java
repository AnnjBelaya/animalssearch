package ua.belaya.animals.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.belaya.animals.animal.domain.Animal;
import ua.belaya.animals.breed.domain.Breed;
import ua.belaya.animals.breed.repository.BreedJpaRepository;
import ua.belaya.animals.breed.service.BreedService;
import ua.belaya.animals.criteria.domain.Criteria;
import ua.belaya.animals.criteria.repository.CriteriaJpaRepository;
import ua.belaya.animals.kind.domain.Kind;
import ua.belaya.animals.location.domain.Location;
import ua.belaya.animals.location.repository.LocationJpaRepository;
import ua.belaya.animals.user.domain.User;
import ua.belaya.animals.animal.repository.AnimalJpaRepository;
import ua.belaya.animals.kind.repository.KindJpaRepository;
import ua.belaya.animals.user.repository.UserJpaRepository;
import ua.belaya.animals.user.service.UserService;

import javax.annotation.PostConstruct;

/**
 * Created by yehor on 24.09.2016.
 */

@Component
public class BootStrap {
    @Autowired
    private UserJpaRepository userJpaRepository;
    @Autowired
    private KindJpaRepository kindJpaRepository;
    @Autowired
    private AnimalJpaRepository animalJpaRepository;
    @Autowired
    private CriteriaJpaRepository criteriaJpaRepository;
    @Autowired
    private BreedJpaRepository breedJpaRepository;
    @Autowired
    private LocationJpaRepository locationJpaRepository;

    @PostConstruct
    public void setUp(){
        User firstUser = new User("asfdsf@mail.ru","first", User.Role.ROLE_ADMIN);
        firstUser.setFirstName("Anna");
        firstUser.setLastName("Belaya");
        firstUser.setPhoneNumber("0504876195");

        User secondUser = new User("scdcdc@mail.ru","second", User.Role.ROLE_USER_ACTIVE);
        secondUser.setFirstName("Yehor");
        secondUser.setLastName("Gulimov");
        secondUser.setPhoneNumber("0504876195");

        User thirdUser = new User("dfcdvf@mail.ru","third", User.Role.ROLE_USER_ACTIVE);
        thirdUser.setFirstName("Juju");
        thirdUser.setLastName("Gulimova");
        thirdUser.setPhoneNumber("0504876195");

        userJpaRepository.save(firstUser);
        userJpaRepository.save(secondUser);
        userJpaRepository.save(thirdUser);

        Kind firstKind = new Kind("Cat");
        Kind secondKind = new Kind("Dog");

        kindJpaRepository.save(firstKind);
        kindJpaRepository.save(secondKind);

        Breed firstBreed = new Breed("Chi-hua-hua");
        Breed secondBreed = new Breed("Dneprovskiy Ushan");
        Breed thirdBreed = new Breed("Pers");
        Breed fourthBreed = new Breed("Buldog");

        firstBreed.setKind(secondKind);
        secondBreed.setKind(firstKind);
        thirdBreed.setKind(firstKind);
        fourthBreed.setKind(secondKind);

        breedJpaRepository.save(firstBreed);
        breedJpaRepository.save(secondBreed);
        breedJpaRepository.save(thirdBreed);
        breedJpaRepository.save(fourthBreed);

        Criteria firstCriteria = new Criteria("White");
        Criteria secondCriteria = new Criteria("Black");
        Criteria thirdCriteria = new Criteria("Sad");

        criteriaJpaRepository.save(firstCriteria);
        criteriaJpaRepository.save(secondCriteria);
        criteriaJpaRepository.save(thirdCriteria);

        Location location = new Location("Dnepr");
        locationJpaRepository.save(location);

        Animal firstAnimal = new Animal(firstUser, 2);
        Animal secondAnimal = new Animal(firstUser, 2);
        Animal thirdAnimal = new Animal(secondUser, 9);
        Animal fourthAnimal = new Animal(secondUser, 10);
        Animal fifthAnimal = new Animal(thirdUser, 15);
        Animal sixthAnimal = new Animal(thirdUser, 3);
        Animal seventhAnimal = new Animal(firstUser, 5);

        firstAnimal.setBreed(firstBreed);
        secondAnimal.setBreed(secondBreed);
        thirdAnimal.setBreed(firstBreed);
        fourthAnimal.setBreed(firstBreed);
        fifthAnimal.setBreed(secondBreed);
        sixthAnimal.setBreed(secondBreed);
        seventhAnimal.setBreed(secondBreed);

        firstAnimal.setKind(firstBreed.getKind());
        secondAnimal.setKind(secondBreed.getKind());
        thirdAnimal.setKind(firstBreed.getKind());
        fourthAnimal.setKind(firstBreed.getKind());
        fifthAnimal.setKind(secondBreed.getKind());
        sixthAnimal.setKind(secondBreed.getKind());
        seventhAnimal.setKind(secondBreed.getKind());

        firstAnimal.getCriteriaList().add(firstCriteria);
        firstAnimal.getCriteriaList().add(secondCriteria);
        secondAnimal.getCriteriaList().add(firstCriteria);
        secondAnimal.getCriteriaList().add(thirdCriteria);
        thirdAnimal.getCriteriaList().add(firstCriteria);
        fourthAnimal.getCriteriaList().add(secondCriteria);
        fifthAnimal.getCriteriaList().add(firstCriteria);
        fifthAnimal.getCriteriaList().add(thirdCriteria);
        sixthAnimal.getCriteriaList().add(firstCriteria);
        seventhAnimal.getCriteriaList().add(thirdCriteria);

        firstAnimal.setLocation(location);
        secondAnimal.setLocation(location);
        thirdAnimal.setLocation(location);
        fourthAnimal.setLocation(location);
        fifthAnimal.setLocation(location);
        sixthAnimal.setLocation(location);
        seventhAnimal.setLocation(location);

        animalJpaRepository.save(firstAnimal);
        animalJpaRepository.save(secondAnimal);
        animalJpaRepository.save(thirdAnimal);
        animalJpaRepository.save(fourthAnimal);
        animalJpaRepository.save(fifthAnimal);
        animalJpaRepository.save(sixthAnimal);
        animalJpaRepository.save(seventhAnimal);
    }
}
