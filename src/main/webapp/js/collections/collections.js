AnimalsSearch.Collections.Users = Backbone.Collection.extend({
    url: "/rest/user"
});

AnimalsSearch.Collections.AllKinds = Backbone.Collection.extend({
    url: "/rest/kind/all"
});

AnimalsSearch.Collections.CriteriaList = Backbone.Collection.extend({
    url: "/rest/criteria"
});

AnimalsSearch.Collections.BreedList = Backbone.Collection.extend({
    url: "/rest/breed/all"
});

AnimalsSearch.Collections.AnimalList = Backbone.Collection.extend({
    url: "/rest/animal"
});

AnimalsSearch.Collections.AnimalListByStatus = Backbone.Collection.extend({
    url: "/rest/animal/status"
});

AnimalsSearch.Collections.AnimalFilteredListByStatusAll = Backbone.Collection.extend({
    url: "/rest/animal/filtered/status/all"
});

AnimalsSearch.Collections.AnimalFilteredListByTwoStatusesAll = Backbone.Collection.extend({
    url: "/rest/animal/filtered/status/two/all"
});

AnimalsSearch.Collections.AnimalListByUsername = Backbone.Collection.extend({
    url: "/rest/animal/username"
});

AnimalsSearch.Collections.AnimalListByUsernameAndStatus = Backbone.Collection.extend({
    url: "/rest/animal/username/status"
});

AnimalsSearch.Collections.LocationList = Backbone.Collection.extend({
    url: "/rest/location/all"
});