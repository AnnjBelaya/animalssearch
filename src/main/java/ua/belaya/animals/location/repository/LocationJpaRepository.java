package ua.belaya.animals.location.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.belaya.animals.location.domain.Location;

/**
 * Created by ann on 08.11.2016.
 */
public interface LocationJpaRepository extends JpaRepository<Location,Long>{
}
