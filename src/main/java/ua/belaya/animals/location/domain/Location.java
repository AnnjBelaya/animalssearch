package ua.belaya.animals.location.domain;

import ua.belaya.animals.core.domain.Domain;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by ann on 08.11.2016.
 */
@Entity
@Table(name = "location")
public class Location implements Domain{
    @Id
    @GeneratedValue
    @Column(name = "location_id")
    private Long id;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[A-z][a-z]+")
    @Column(name = "location_name", unique = true, nullable = false)
    private String name;

    public Location(){}

    public Location(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (!name.equals(location.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
