AnimalsSearch.Models.User = Backbone.Model.extend({
    urlRoot: "/rest/user"
});

AnimalsSearch.Models.UserByUsername = Backbone.Model.extend({
    urlRoot: "/rest/user/username"
});

AnimalsSearch.Models.Kind = Backbone.Model.extend({
    urlRoot: "/rest/kind"
});

AnimalsSearch.Models.Animal = Backbone.Model.extend({
    urlRoot: "/rest/animal"
});

AnimalsSearch.Models.Criteria = Backbone.Model.extend({
    urlRoot: "/rest/criteria"
});

AnimalsSearch.Models.Breed = Backbone.Model.extend({
    urlRoot: "/rest/breed"
});

AnimalsSearch.Models.NumberOfAnimals = Backbone.Model.extend({
    urlRoot: "/rest/animal/numberOfAnimals"
});

AnimalsSearch.Models.NumberOfAnimalsByStatus = Backbone.Model.extend({
    urlRoot: "/rest/animal/numberOfAnimals/status"
});

AnimalsSearch.Models.NumberOfAnimalsByUsername = Backbone.Model.extend({
    urlRoot: "/rest/animal/numberOfAnimals/username"
});

AnimalsSearch.Models.NumberOfAnimalsByStatusAndUsername = Backbone.Model.extend({
    urlRoot: "/rest/animal/numberOfAnimals/username/status"
});

AnimalsSearch.Models.AuthorizedUser = Backbone.Model.extend({
    urlRoot: "/rest/user/authorized"
});

AnimalsSearch.Models.Location = Backbone.Model.extend({
    urlRoot: "/rest/location"
});

AnimalsSearch.Models.Message = Backbone.Model.extend({
    urlRoot: "/rest/user/message"
});

AnimalsSearch.Models.AuthoriseUser = Backbone.Model.extend({
    urlRoot: "/rest/user/activate"
});

AnimalsSearch.Models.Photo = Backbone.Model.extend({
    urlRoot: "/rest/animal/photo"
});



