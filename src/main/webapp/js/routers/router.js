AnimalsSearch.Routes.Router = Backbone.Router.extend({
    routes: {
        '' : "welcome",
        'users': 'users',
        'user/animal/add' : 'addAnimalPage',
        'user/profile' : 'userProfile',
        'user/profile/change' : 'changeUserProfile',
        'animals/all' : 'allAnimals',
        'animals/my' : 'myAnimals',
        'animal/find/:id' : 'findAnimal',
        'user/:id' : 'anotherUserProfile',
        'user/activate/:id' : 'authoriseUser',
        '/rest/animal/:id/photo' : 'refreshView'
    }
});
