package ua.belaya.animals.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.belaya.animals.user.domain.User;
import ua.belaya.animals.user.service.UserService;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.List;

/**
 * @author Anna Belaya
 */
@RestController
@RequestMapping("/rest/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/authorized", method = RequestMethod.GET)
    public ResponseEntity<User> getAuthorizedUser() {
        return new ResponseEntity<User>(userService.getAuthorized(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> add(@Valid @RequestBody User user) {
        return new ResponseEntity<User>(userService.add(user), HttpStatus.OK);
    }

    @RequestMapping(value = "/username/{username}", method = RequestMethod.GET)
    public ResponseEntity<User> getByUsername(@PathVariable("username") String username) {
        return new ResponseEntity<User>(userService.getByUsername(username), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> get(@PathVariable("id") Long id) {
        return new ResponseEntity<User>(userService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@PathVariable("id") Long id,
                                       @Valid @RequestBody User user) {
        userService.update(id, user);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        userService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<User> list(@PathParam("page") int page, @PathParam("size") int size) {
        return userService.list(new PageRequest(page, size)).getContent();
    }

    @RequestMapping(value = "/message/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> sendMessage(@PathVariable("id") Long id,
                                            @PathParam("subject") String subject,
                                            @PathParam("text") String text) {
        return new ResponseEntity<User>(userService.sendMessage(id, subject, text), HttpStatus.OK);
    }

    @RequestMapping(value = "/activate/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> activate(@PathVariable("id") Long id){
        userService.activate(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
