package ua.belaya.animals.breed.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ua.belaya.animals.breed.domain.Breed;
import ua.belaya.animals.breed.domain.BreedDTO;
import ua.belaya.animals.breed.repository.BreedJpaRepository;
import ua.belaya.animals.core.domain.Domains;
import ua.belaya.animals.core.exception.DomainNotFoundException;
import ua.belaya.animals.core.exception.DuplicateDomainException;

import java.util.ArrayList;
import java.util.List;

@Service
public class BreedServiceImpl implements BreedService {
    @Autowired
    private BreedJpaRepository breedRepository;

    @Override
    public Breed add(Breed breed) {
        try {
            return breedRepository.save(breed);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateDomainException(Domains.BREED);
        }
    }

    @Override
    public BreedDTO getByName(String name) {
        Breed breed = breedRepository.getByName(name);

        if (breed == null) {
            throw new DomainNotFoundException(Domains.BREED, name);
        }

        return new BreedDTO(breed);
    }

    @Override
    public BreedDTO get(Long id) {
        Breed breed = breedRepository.findOne(id);

        if (breed == null) {
            throw new DomainNotFoundException(Domains.BREED, id);
        }

        return new BreedDTO(breed);
    }

    @Override
    public Breed update(Long id, Breed breed) {
        try {
            breed.setId(id);
            return breedRepository.save(breed);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateDomainException(Domains.BREED);
        }
    }

    @Override
    public void delete(Long id) {
        Breed breed = breedRepository.findOne(id);

        if (breed == null) {
            throw new DomainNotFoundException(Domains.BREED, id);
        }

        breedRepository.delete(breed);
    }

    @Override
    public List<BreedDTO> list() {
        List<BreedDTO> breedDTOList = new ArrayList<BreedDTO>();

        for (Breed breed : breedRepository.findAll()){
            breedDTOList.add(new BreedDTO(breed));
        }

        return breedDTOList;
    }

    @Override
    public List<BreedDTO> list(Pageable pageable) {
        List<BreedDTO> breedList = new ArrayList<BreedDTO>();

        for (Breed breed : breedRepository.list(pageable)){
            breedList.add(new BreedDTO(breed));
        }

        return breedList;
    }
}
