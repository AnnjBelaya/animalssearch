package ua.belaya.animals.criteria.service;

import ua.belaya.animals.core.service.DomainService;
import ua.belaya.animals.criteria.domain.Criteria;
import java.util.List;

public interface CriteriaService extends DomainService<Criteria>{
    Criteria get(Long id);

    Criteria getByName(String name);

    List<Criteria> list();
}
