package ua.belaya.animals.criteria.domain;

import ua.belaya.animals.animal.domain.Animal;
import ua.belaya.animals.core.domain.Domain;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "criteria")
public class Criteria implements Domain{
    @Id
    @GeneratedValue
    @Column(name = "criteria_id")
    private Long id;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[a-zA-Z-\\s]+")
    @Column(name = "criteria_name", unique = true, nullable = false)
    private String name;

    public Criteria(){}

    public Criteria(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Criteria criteria = (Criteria) o;

        if (!name.equals(criteria.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
