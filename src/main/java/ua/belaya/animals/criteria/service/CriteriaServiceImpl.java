package ua.belaya.animals.criteria.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import ua.belaya.animals.core.domain.Domains;
import ua.belaya.animals.core.exception.DomainNotFoundException;
import ua.belaya.animals.core.exception.DuplicateDomainException;
import ua.belaya.animals.criteria.domain.Criteria;
import ua.belaya.animals.criteria.repository.CriteriaJpaRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CriteriaServiceImpl implements CriteriaService{
    @Autowired
    private CriteriaJpaRepository criteriaRepository;

    @Override
    public Criteria add(Criteria criteria) {
        try {
            return criteriaRepository.save(criteria);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateDomainException(Domains.CRITERIA);
        }
    }

    @Override
    public Criteria getByName(String name) {
        Criteria criteria = criteriaRepository.getByName(name);

        if (criteria == null) {
            throw new DomainNotFoundException(Domains.CRITERIA, name);
        }

        return criteria;
    }

    @Override
    public Criteria get(Long id) {
        Criteria criteria = criteriaRepository.findOne(id);

        if (criteria == null) {
            throw new DomainNotFoundException(Domains.CRITERIA, id);
        }

        return criteria;
    }

    @Override
    public Criteria update(Long id, Criteria criteria) {
        try {
            criteria.setId(id);
            return criteriaRepository.save(criteria);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateDomainException(Domains.CRITERIA);
        }
    }

    @Override
    public void delete(Long id) {
        Criteria criteria = criteriaRepository.findOne(id);

        if (criteria == null) {
            throw new DomainNotFoundException(Domains.CRITERIA, id);
        }

        criteriaRepository.delete(criteria);
    }

    @Override
    public List<Criteria> list() {
        return criteriaRepository.findAll();
    }
}
